#define O_RDONLY      0x000
#define O_WRONLY      0x001
#define O_RDWR        0x002
/* CMPT 332 GROUP 34 Change, Fall 2019 */
#define O_NOFOLLOW    0x004
/*--------End Modifications-----------*/
#define O_CREATE      0x200
