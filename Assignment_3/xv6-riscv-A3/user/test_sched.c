#include "kernel/types.h"
#include "kernel/spinlock.h"
#include "kernel/param.h"
#include "kernel/stat.h"
#include "user/user.h"
#include "kernel/fs.h"
#include "kernel/fcntl.h"
#include "kernel/syscall.h"
#include "kernel/memlayout.h"
#include "kernel/riscv.h"
#include "kernel/proc.h"

#define NLOOP  60000
#define SQUARE1  1500
#define SQUARE2  600

int
square(){
  int y=1;
  int i,j,k;

  for(i=0; i<NLOOP; i++){
    for(j=0; j<SQUARE1; j++){
      y=1;
      for(k=1; k<20; k++){
        y = y * k;
      }
    }
  }  
  
  return y;
}

int
square2(){
  int y=1;
  int i,j,k;

  for(i=0; i<NLOOP; i++){
    for(j=0; j<SQUARE2; j++){
      y=1;
      for(k=1; k<20; k++){
        y = y * k;
      }
    }
  }  
  
  return y;
}

void
main (int argc, char *argv[]) {  
    int addr,i;
 
    if (fork() == 0){
        printf("priority at start:\n");
        printprio();
        printf("square: %d\n", square());
        sleep(50);
        printf("priority after sleep:\n");
        printprio();
        nice(1);
        printf("priority after nice(1):\n");
        printprio();
        exit(1);
    }

    if (fork() == 0){
        printf("priority at start:\n");
        printprio();
        printf("square: %d\n", square2());
        sleep(50);
        printf("priority after sleep:\n");
        printprio();
        nice(-1);
        printf("priority after nice(-1):\n");
        printprio();
        exit(1);
    }  

    for (i=0; i<2;i++)
        wait(&addr);
    exit(1);
}



