/* NAME:Musquer Basile, Boehm Maximilian
 * NSID:bam857, mab728
 * Student Number:11287646, 11288097
 * Group: 34
 *
 * University of Saskatchewan
 * CMPT 332 Fall 2019
 * File: klist.c
 * Assignment 3
*/
/************************** PRE-SETs ******************************/
/* Include */
#include "types.h"
#include "param.h"
#include "memlayout.h"
#include "spinlock.h"
#include "riscv.h"
#include "defs.h"
#include "stddef.h"

/* Node variables */
Node chunkNode_pt[NPROC];
NodeA arrayNodeA[NPROC];
NodeA* currNA;
static unsigned int initNode = 0;

/* List varaibles */
List chunkList_pt[NPRIOQ];
ListA arrayListA[NPRIOQ];
ListA* currLA=NULL;
static unsigned int initList=0;

/********************STATIC FUNCTIONS ******************************/
/******************************************************************/
/*-------------------------------*//*List Properly Initialize*/
static inline int ListPropInit( List* list ) {
  if ( list->curr == NULL && list->last == NULL && list->first == NULL ) {
    return 1;
  }

  if ( list->curr != NULL && list->last != NULL && list->first != NULL ) {
    return 2;
  }

  printf( "List not properly initialize\n" );
  return 0;
}

/*-------------------------------*//*Node Available*/
static inline Node* nodeAvailable() {
  if ( chunkNode_pt ==  NULL ){
    panic("chunkNode_pt not initialized");
  }
  if ( arrayNodeA == NULL ){
    panic("arrayNodeA not initialized");
  }
  Node* tmpNode = NULL;
  NodeA* tmpNodeA = NULL;

  /* Begining Initialisation*/
  unsigned int i = 0;
  if ( initNode == 0 ) {
    for(tmpNodeA = arrayNodeA, tmpNode = chunkNode_pt, i = 0;  tmpNodeA < 
            (arrayNodeA + NPROC - 1); i++, tmpNodeA++, tmpNode++ ) {
      tmpNodeA->ptrNode = tmpNode;
      tmpNode->pos = i;
      tmpNodeA->nextNA = tmpNodeA + 1;
    }
    tmpNodeA->ptrNode = tmpNode;
    tmpNode->pos = i;
    tmpNodeA->nextNA = NULL;
    currNA = arrayNodeA;    

    initNode = 1;
  }

  /* Test Maximum */
  if ( currNA == NULL ) {
    printf( "Maximum number of nodes reaches\n" );
    return NULL;
  } 


  /* Find available node */
  tmpNode = currNA->ptrNode;
  tmpNodeA = currNA;
  currNA = currNA->nextNA;
  tmpNodeA->nextNA = NULL;

  return tmpNode;
}

/************************** FUNCTIONS ******************************/
/******************************************************************/
/*-------------------------------*//*List Prepend*/

int ListPrepend( List* list, void* item ) {
  if ( list ==  NULL ){
    panic("arg list is NULL");
  }
  Node* tmpNode = nodeAvailable();
  if ( tmpNode == NULL ) return -1;
  
  /* Case empty list */
  if ( list->curr == NULL ) {
    list->first = tmpNode;
    list->last = tmpNode;
    tmpNode->prev = NULL;
    tmpNode->next = NULL;
  }
  else {/* Normal case: the new node is put at the begining of a non empty 
            list*/

    /* Check if list->first is not null, if curr is not null, 
        first should not be too tho */
    if ( list->first == NULL) {
      printf( "List not properly initialize\n" );
      return -1;
    }

    list->first->prev = tmpNode;
    tmpNode->next = list->first;
    tmpNode->prev = NULL;
    list->first = tmpNode;
  }

  tmpNode->item = item;
  list->curr = tmpNode;
  list->nbNode ++;

  return 0;
}

/*-------------------------------*//*List Create*/
List* ListCreate() {
  if ( chunkList_pt == NULL ){
    panic("chunkList_pt not initialized");
  }
  if ( arrayListA == NULL ){
    panic("arrayListA not initialized");
  }

  List* tmpList = NULL;
  ListA* tmpListA = NULL;

  /* Begining Initialisation */
  int i=0;
  if ( initList == 0 ) {
    for ( tmpListA = arrayListA, tmpList = chunkList_pt, i = 0; tmpListA 
                < (arrayListA + NPRIOQ - 1); tmpListA++, tmpList++, i++ ) {
      tmpListA->ptrList = tmpList;
      tmpList->locate = i;
      tmpListA->nextLA = tmpListA + 1;
    }
    tmpListA->ptrList = tmpList;
    tmpList->locate = i;
    tmpListA->nextLA = NULL;
    currLA = arrayListA;

    initList = 1;
  }

  /* Test Maximum */
  if ( currLA == NULL ) {
    printf( "Max number of lists reach, could not create a new one\n" );
    return NULL;
  }

  /* Find available list */
  tmpList = currLA->ptrList;
  tmpListA = currLA;
  currLA = currLA->nextLA;
  tmpListA->nextLA = NULL;

  if ( tmpList == NULL ) {
    printf( "Link btw pool lists and arr of available lists failed\n" );
    return tmpList;
  } 
  
  //Variables Initialisation
  tmpList->curr = NULL;
  tmpList->first = NULL;
  tmpList->last = NULL;
  tmpList->nbNode = 0;

  return tmpList;
}

/*-------------------------------*//*List Trim*/

void* ListTrim( List* list ) {
  if ( list ==  NULL ){
    panic("arg list is NULL");
  }
  Node* tmp = list->last;

  /* Removing the node from the list*/
  if ( list->curr == NULL ) {//Case list is empty
    return NULL;
  }
  else if ( list->nbNode == 1 ) {//Case last is the only node
    list->curr = NULL;
    list->last = NULL;
    list->first = NULL;
  }
  else {//Case more than 1 node
    list->last = list->last->prev;
    list->last->next = NULL;
    list->curr = list->last;
  }

  list->nbNode--;
  /* Making the node available again  */
  arrayNodeA[( tmp->pos )].nextNA = currNA;
  currNA = &arrayNodeA[( tmp->pos )];
  

  return tmp->item;
}


/*-------------------------------*//*List Count*/
int ListCount( List* list ) {
  if ( list ==  NULL ){
    panic("arg list is NULL");
  }
  return list->nbNode;
}

/*-------------------------------*//*List Last*/
void* ListLast( List* list ) {
  if ( list ==  NULL ){
    panic("arg list is NULL");
  }
  int test = 0;

  test = ListPropInit( list );  

  /* List Non properly initialize or empty */
  if ( ( test == 0 ) || ( test == 1 ) ) {
    return NULL;
  }

  list->curr = list->last;

  return list->curr->item;
}

