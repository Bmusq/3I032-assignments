// Physical memory allocator, for user processes,
// kernel stacks, page-table pages,
// and pipe buffers. Allocates whole 4096-byte pages.

#include "types.h"
#include "param.h"
#include "memlayout.h"
#include "spinlock.h"
#include "riscv.h"
#include "defs.h"

void freerange(void *pa_start, void *pa_end);

extern char end[]; // first address after kernel.
                   // defined by kernel.ld.

struct run {
  struct run *next;
};

/* CMPT 332 GROUP 34 Change, Fall 2019 */
char rcpte[(PHYSTOP - KERNBASE) / PGSIZE]; // Array of reference counter of pte
struct spinlock lock_rcpte;
/*--------End Modifications-----------*/

struct {
  struct spinlock lock;
  struct run *freelist;
/* CMPT 332 GROUP 34 Change, Fall 2019 */
  int nfreepa;  // Number of free pages
/*--------End Modifications-----------*/
} kmem;

void
kinit()
{
  initlock(&kmem.lock, "kmem");
/* CMPT 332 GROUP 34 Change, Fall 2019 */
  initlock(&lock_rcpte, "rcpte");

  acquire(&kmem.lock);
  kmem.nfreepa = 0;
  release(&kmem.lock);
/*--------End Modifications-----------*/
  freerange(end, (void*)PHYSTOP);
}

void
freerange(void *pa_start, void *pa_end)
{
  char *p;
  p = (char*)PGROUNDUP((uint64)pa_start);
  for(; p + PGSIZE <= (char*)pa_end; p += PGSIZE){
/* CMPT 332 GROUP 34 Change, Fall 2019 */
    acquire(&kmem.lock);
    kmem.nfreepa ++;
    release(&kmem.lock);
/*--------End Modifications-----------*/
    kfree(p);
  }
}

// Free the page of physical memory pointed at by v,
// which normally should have been returned by a
// call to kalloc().  (The exception is when
// initializing the allocator; see kinit above.)
void
kfree(void *pa)
{
  struct run *r;

  if(((uint64)pa % PGSIZE) != 0 || (char*)pa < end || (uint64)pa >= PHYSTOP)
    panic("kfree");
  
  // Fill with junk to catch dangling refs.
  memset(pa, 1, PGSIZE);

  r = (struct run*)pa;

/* CMPT 332 GROUP 34 Change, Fall 2019 */
  acquire(&lock_rcpte);
  rcpte[PGIND(pa)] = 0;
  release(&lock_rcpte);

  acquire(&kmem.lock);
  kmem.nfreepa ++;
  r->next = kmem.freelist;
  kmem.freelist = r;
  release(&kmem.lock);
/*--------End Modifications-----------*/
}

// Allocate one 4096-byte page of physical memory.
// Returns a pointer that the kernel can use.
// Returns 0 if the memory cannot be allocated.
void *
kalloc(void)
{
  struct run *r;

  acquire(&kmem.lock);
  r = kmem.freelist;
  if(r){
    kmem.freelist = r->next;
/* CMPT 332 GROUP 34 Change, Fall 2019 */
    kmem.nfreepa --;

    acquire(&lock_rcpte);
    rcpte[PGIND(r)] = 1;
    release(&lock_rcpte);
/*--------End Modifications-----------*/
  }
  release(&kmem.lock);


  if(r){
    memset((char*)r, 5, PGSIZE); // fill with junk
  }

  return (void*)r;
}

/* CMPT 332 GROUP 34 Change, Fall 2019 */
// System call that returns the number of free pages
int
getNumFreePages(void){
  acquire(&kmem.lock);
  int r = kmem.nfreepa;
  release(&kmem.lock);

  return r;
}
/*--------End Modifications-----------*/




