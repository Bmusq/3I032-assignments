/* NAME:Musquer Basile, Boehm Maximilian
   NSID:bam857, mab728
   Student Number:11287646, 11288097
   Group: 34

   University of Saskatchewan
   CMPT 332 Term 1 2019
   Assignment 2
   Part B
*/

/************************** PRE-SETs ******************************/
/* Include */
#include <monitor.h>
#include <os.h>
#include <standards.h>

/* Define */
#define numConds  2
#define OKtoRead  2
#define OKtoWrite 3

/* Readers/Writers tracking variables */
static int numReaders = 0;
static int busyWriting = 0;
static int waitingWriters = 0;
static int waitingReaders = 0;

/************************** MONITOR PROCEDURES ******************************/
/***************************************************************************/
/*-------------------------------*//*Call Initialisation*/
void Initialize(void)
{
      MonInit(numConds);
}

/*-------------------------------*//*Read the CS*/
void StartRead()
{
	/* Enter the Monitor */
  MonEnter();

  /* If there is a writer writing or a writer waiting: placed on the readers waitingQ */
  if (busyWriting || waitingWriters)
    {
		waitingReaders ++;
    	MonWait(OKtoRead);
		waitingReaders --;
    }
	/* Number of readers increase by one */
  numReaders++;
  printf("starting value of numReaders = %d\n", numReaders);

	/* Signal any waiting readers on the waitingQ that they can resume execution */
  MonSignal(OKtoRead);

	/* Leave the Monitor */
  MonLeave();
}

/*-------------------------------*//*Stop reading CS*/
void StopRead()
{
	/* Enter the Monitor*/
  MonEnter();
	
	/* decrease number of readers */
  numReaders--;
  printf("stopping value of numReaders = %d\n", numReaders);

 	/* Only signal the writer when all readers have gone */
  if (numReaders == 0) MonSignal(OKtoWrite);

	/* Leave the Monitor */
  MonLeave();
}

/*-------------------------------*//*Write in the CS*/
void StartWrite()
{
  MonEnter(); 

  /* If there is a writer writing or readers reading */
	if ((numReaders !=0) || busyWriting) {
		waitingWriters++;
		MonWait(OKtoWrite);
		waitingWriters--;	
	}
	busyWriting = 1;
	MonLeave();
}

/*-------------------------------*//*Stop writing in the CS*/
void StopWrite()
{
  MonEnter();

  busyWriting = 0;

	if(waitingReaders>0)
	  MonSignal(OKtoRead);
	else
		MonSignal(OKtoWrite);
  MonLeave();
}
