/* NAME:Musquer Basile, Boehm Maximilian
 * NSID:bam857, mab728
 * Student Number:11287646, 11288097
 * Group: 34
 *
 * University of Saskatchewan
 * CMPT 332 Fall 2019
 * File: LibList:list_movers.c
*/

/************************** PRE-SETs ******************************/
/* Include */
#include <list.h>


/************************** FUNCTIONS ******************************/
/******************************************************************/
/*-------------------------------*//*List Properly Initialize*/
static inline int ListPropInit( List* list ) {
	if ( list->curr == NULL && list->last == NULL && list->first == NULL ) {
		return 1;
	}

	if ( list->curr != NULL && list->last != NULL && list->first != NULL ) {
		return 2;
	}

	printf( "List not properly initialize\n" );
	return 0;
}

/*-------------------------------*//*List Count*/
int ListCount( List* list ) {
	assert( list !=  NULL );
	return list->nbNode;
}

/*-------------------------------*//*List First*/
void* ListFirst( List* list ) {
	assert( list !=  NULL );
	int test = 0;

	test = ListPropInit( list );	

	/* List Non properly initialize or empty */
	if ( ( test == 0 ) || ( test == 1 ) ) {
		return NULL;
	}

	/* Otherwise proceed */
	list->curr = list->first;

	return list->curr->item;
}

/*-------------------------------*//*List Last*/
void* ListLast( List* list ) {
	assert( list !=  NULL );
	int test = 0;

	test = ListPropInit( list );	

	/* List Non properly initialize or empty */
	if ( ( test == 0 ) || ( test == 1 ) ) {
		return NULL;
	}

	list->curr = list->last;

	return list->curr->item;
}

/*-------------------------------*//*List Next*/
void* ListNext( List* list ) {
	assert( list !=  NULL );
	int test = 0;

	test = ListPropInit( list );	

	/* List Non properly initialize or empty */
	if ( ( test == 0 ) || ( test == 1 ) ) {
		return NULL;
	}

	if ( list->curr->next  ==  NULL ) return NULL;
	list->curr = list->curr->next;

	return list->curr->item;
}

/*-------------------------------*//*List Previous*/
void* ListPrev( List* list ) {
	assert( list !=  NULL );
	int test = 0;

	test = ListPropInit( list );	

	/* List Non properly initialize or empty */
	if ( ( test == 0 ) || ( test == 1 ) ) {
		return NULL;
	}

	if ( list->curr->prev  ==  NULL ) return NULL;

	list->curr = list->curr->prev;
	return list->curr->item ;
}

/*-------------------------------*//*List Current*/
void* ListCurr( List* list ) {
	assert( list !=  NULL );
	int test = 0;

	test = ListPropInit( list );	

	/* List Non properly initialize or empty */
	if ( ( test == 0 ) || ( test == 1 ) ) {
		return NULL;
	}

	return list->curr->item;
}

/*-------------------------------*//*List Search*/
void* ListSearch( List* list,int ( comparator )( void*, void* ), void* comparisonArg ) {
	assert( list != NULL );	
	int test = 0;

	test = ListPropInit( list );	

	/* List Non properly initialize or empty */
	if ( ( test == 0 ) || ( test == 1 ) ) {
		return NULL;
	}

	/* Searches in the list from current item*/
	while ( list->curr !=  NULL ) {
		/* Try match*/
		if ( comparator( list->curr->item, comparisonArg ) ) {
			return list->curr->item;
		}
		list->curr = list->curr->next;
	}

	list->curr = list->last;
	return NULL;
}


