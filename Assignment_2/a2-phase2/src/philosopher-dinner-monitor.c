/* NAME:Musquer Basile, Boehm Maximilian
   NSID:bam857, mab728
   Student Number:11287646, 11288097
   Group: 34

   University of Saskatchewan
   CMPT 332 Term 1 2019
   Assignment 2
   Part B
*/

/************************** PRE-SETs ******************************/
/* Include */
#include <monitor.h>
#include <os.h>
#include <standards.h>

/* Define */
#define numConds  5

/* Typedef */
enum State { Hungry = 0, Eating = 1, Thinking = 2};

static enum State state[numConds];
static int numQ[numConds]; 

/************************** MONITOR PROCEDURES ******************************/
/***************************************************************************/
/*-------------------------------*//*Call Initialisation*/
void Initialize(void)
{
	int i;
	MonInit(numConds);
	for(i = 0; i < numConds; i++) {
		state[i]=Thinking;
		if(i == 0)
			numQ[i]=5;
		else if(i == 1)
			numQ[i]=6;
		else
			numQ[i]=i;
	}
}
/*-------------------------------*//*Test*/
void test(int id){
	if ((state[(id-1)%5] != Eating) && (state[id%5] == Hungry) && (state[(id+1)%5] != Eating)) {
		state[id%5] = Eating;
		MonSignal(numQ[id%5]);
	}
	return ;
}

/*-------------------------------*//*Pick up Fork*/
void pickUp(int id){
	MonEnter();
	
	state[id%5] = Hungry;
	test(id);

	if(state[id%5] != Eating) {
		printf("Philosopher %d is waiting to eat\n", id);
		MonWait(numQ[id%5]);
	}
	
	MonLeave();
	return ;
}

/*-------------------------------*//*Put down Fork*/
void putDown(int id){
	MonEnter();
	state[id%5] = Thinking;
	test((id-1)%5);
	test((id+1)%5);
	MonLeave();
	return ;
}






