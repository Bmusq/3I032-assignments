/* NAME:Musquer Basile, Boehm Maximilian
 * NSID:bam857, mab728
 * Student Number:11287646, 11288097
 * Group: 34
 *
 * University of Saskatchewan
 * CMPT 332 Fall 2019
 * File: LibList:list_adders.c
*/

/************************** PRE-SETs ******************************/
/* Include */
#include <list.h>

/* Array of Node struct */
Node chunkNode_pt[MAX_NODE];

/* Array of Node available */
NodeA arrayNodeA[MAX_NODE];

/* Pointer current available node */
NodeA* currNA;

/* Init */
static unsigned int initNode = 0;


/************************** FUNCTIONS ******************************/
/******************************************************************/

/*-------------------------------*//*Node Available*/
static inline Node* nodeAvailable() {
	assert( chunkNode_pt !=  NULL );
	assert( arrayNodeA != NULL );
	Node* tmpNode = NULL;
	NodeA* tmpNodeA = NULL;

	/* Begining Initialisation*/
	unsigned int i = 0;
	if ( initNode == 0 ) {
		for(tmpNodeA = arrayNodeA, tmpNode = chunkNode_pt, i = 0;  tmpNodeA < (arrayNodeA + MAX_NODE - 1); i++, tmpNodeA++, tmpNode++ ) {
			tmpNodeA->ptrNode = tmpNode;
			tmpNode->pos = i;
			tmpNodeA->nextNA = tmpNodeA + 1;
		}
		tmpNodeA->ptrNode = tmpNode;
		tmpNode->pos = i;
		tmpNodeA->nextNA = NULL;
		currNA = arrayNodeA;		

		initNode = 1;
	}

	/* Test Maximum */
	if ( currNA == NULL ) {
		printf( "Maximum number of nodes reaches\n" );
		return NULL;
	}	


	/* Find available node */
	tmpNode = currNA->ptrNode;
	tmpNodeA = currNA;
	currNA = currNA->nextNA;
	tmpNodeA->nextNA = NULL;

	return tmpNode;
}

/*-------------------------------*//*List Add*/

int ListAdd( List* list, void* item ) {
	assert( list !=  NULL );
	Node* tmpNode = nodeAvailable();
	if ( tmpNode == NULL ) return -1;
	
	/* Case empty list */
	if ( list->curr == NULL ) {
		list->first = tmpNode;
		list->last = tmpNode;
		tmpNode->prev = NULL;
		tmpNode->next = NULL;
	}/* Case current node is last */
	else if ( list->curr->next == NULL ) {
		list->curr->next = tmpNode;
		tmpNode->prev = list->curr;
		tmpNode->next = NULL;
		list->last = tmpNode;
	}
	else {/* Case current node is in the middle */
		tmpNode->next = list->curr->next;
		list->curr->next->prev = tmpNode;
		list->curr->next = tmpNode;
		tmpNode->prev = list->curr;
	}

	tmpNode->item = item;
	list->curr = tmpNode;
	list->nbNode ++;

	return 0;
}

/*-------------------------------*//*List Insert*/

int ListInsert( List* list, void* item ) {
	assert( list !=  NULL );
	Node* tmpNode = nodeAvailable();
	if ( tmpNode == NULL ) return -1;
	
	/* Case empty list */
	if ( list->curr == NULL ) {
		list->first = tmpNode;
		list->last = tmpNode;
		tmpNode->prev = NULL;
		tmpNode->next = NULL;
	}/* Case current node is first */
	else if ( list->curr->prev == NULL ) {
		list->curr->prev = tmpNode;
		tmpNode->next = list->curr;
		tmpNode->prev = NULL;
		list->first = tmpNode;
	}
	else {/* Case current node is in the middle */
		tmpNode->next = list->curr;
		list->curr->prev->next = tmpNode;
		tmpNode->prev = list->curr->prev;
		list->curr->prev = tmpNode;
	}

	tmpNode->item = item;
	list->curr = tmpNode;
	list->nbNode ++;

	return 0;
}

/*-------------------------------*//*List Append*/

int ListAppend( List* list, void* item ) {
	assert( list !=  NULL );
	Node* tmpNode = nodeAvailable();
	if ( tmpNode == NULL ) return -1;
	
	/* Case empty list */
	if ( list->curr == NULL ) {
		list->first = tmpNode;
		list->last = tmpNode;
		tmpNode->prev = NULL;
		tmpNode->next = NULL;
	}
	else {/* Normal case: the new node is put at the end of a non empty list*/

		/* Check if list->last is not null, if curr is not null, last should not be too tho */
		if ( list->last == NULL) {
			printf( "List not properly initialize\n" );
			return -1;
		}

		list->last->next = tmpNode;
		tmpNode->prev = list->last;
		tmpNode->next = NULL;
		list->last = tmpNode;
	}

	tmpNode->item = item;
	list->curr = tmpNode;
	list->nbNode ++;

	return 0;
}

/*-------------------------------*//*List Prepend*/

int ListPrepend( List* list, void* item ) {
	assert( list !=  NULL );
	Node* tmpNode = nodeAvailable();
	if ( tmpNode == NULL ) return -1;
	
	/* Case empty list */
	if ( list->curr == NULL ) {
		list->first = tmpNode;
		list->last = tmpNode;
		tmpNode->prev = NULL;
		tmpNode->next = NULL;
	}
	else {/* Normal case: the new node is put at the begining of a non empty list*/

		/* Check if list->first is not null, if curr is not null, first should not be too tho */
		if ( list->first == NULL) {
			printf( "List not properly initialize\n" );
			return -1;
		}

		list->first->prev = tmpNode;
		tmpNode->next = list->first;
		tmpNode->prev = NULL;
		list->first = tmpNode;
	}

	tmpNode->item = item;
	list->curr = tmpNode;
	list->nbNode ++;

	return 0;
}

