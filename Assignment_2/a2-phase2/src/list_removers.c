/* NAME:Musquer Basile, Boehm Maximilian
 * NSID:bam857, mab728
 * Student Number:11287646, 11288097
 * Group: 34
 *
 * University of Saskatchewan
 * CMPT 332 Fall 2019
 * File: LibList:list_removers.c
*/

/************************** PRE-SETs ******************************/
/* Include */
#include <list.h>

/* List extern variables */
extern ListA* currLA;
extern ListA arrayListA[MAX_LIST];

/* Node extern variables */
extern NodeA* currNA;
extern NodeA arrayNodeA[MAX_NODE];

/************************** FUNCTIONS ******************************/
/******************************************************************/

/*-------------------------------*//*List Free*/
void ListFree( List* list, void ( *itemFree )( void* ) ) {
	assert( list !=  NULL );
	list->curr = list->first;	
	
	/* Make each node of the list available */
	while( list->curr !=  NULL ) {
		itemFree( list->curr->item );
		arrayNodeA[( list->curr->pos )].nextNA = currNA;
		currNA = &arrayNodeA[( list->curr->pos )];

		list->curr = list->curr->next;
	}	

	/* Resetting list variables ( Might be obsolete ) */
	list->last = NULL;
	list->first = NULL;
	list->nbNode = 0;

	/* Make the list available */
	arrayListA[( list->locate )].nextLA = currLA;
	currLA = &arrayListA[( list->locate )];

	return ;
}

/*-------------------------------*//*List Trim*/

void* ListTrim( List* list ) {
	assert( list !=  NULL );
	Node* tmp = list->last;

	/* Removing the node from the list*/
	if ( list->curr == NULL ) {//Case list is empty
		return NULL;
	}
	else if ( list->nbNode == 1 ) {//Case last is the only node
		list->curr = NULL;
		list->last = NULL;
		list->first = NULL;
	}
	else {//Case more than 1 node
		list->last = list->last->prev;
		list->last->next = NULL;
		list->curr = list->last;
	}

	list->nbNode--;
	/* Making the node available again  */
	arrayNodeA[( tmp->pos )].nextNA = currNA;
	currNA = &arrayNodeA[( tmp->pos )];
	

	return tmp->item;
}

/*-------------------------------*//*List Remove*/

void* ListRemove( List* list ) {
	assert( list !=  NULL );
	Node* tmp = list->curr;
	
	/* Removing the node from the list */
	if ( list->curr == NULL ) {//Case list is empty
		return NULL;
	}
	else if ( list->nbNode == 1 ) {//Case curr is the only node
		list->curr = NULL;
		list->last = NULL;
		list->first = NULL;
	}
	else if ( list->curr->next == NULL ) {//Case curr is last

		if ( list->curr->prev == NULL ) {
			printf( "List not properly formed\n" );
			return NULL;
		}

		list->curr->prev->next = NULL;
		list->curr = list->curr->prev;
		list->last = list->curr;
	}
	else if ( list->curr->prev == NULL ) {//Case curr is first

		if ( list->curr->next == NULL ) {
			printf( "List not properly formed\n" );
			return NULL;
		}

		list->curr->next->prev = NULL;
		list->curr = list->curr->next;
		list->first = list->curr;
	}
	else {//Case curr is in the middle of the list

		if ( ( list->curr->next == NULL ) || ( list->curr->prev == NULL ) ) {
			printf( "List not properly formed\n" );
			return NULL;
		}

		list->curr->next->prev = list->curr->prev;
		list->curr->prev->next = list->curr->next;
		list->curr = list->curr->next;
	}

	/* Making the node available again  */
	arrayNodeA[( tmp->pos )].nextNA = currNA;
	currNA = &arrayNodeA[( tmp->pos )];
	list->nbNode --;

	return tmp->item;
}

/*-------------------------------*//*List Concat*/

void ListConcat( List* list1, List* list2 ) {
	assert( list1 !=  NULL && list2!= NULL );
	
	if ( !( ( list1->curr == NULL && list1->last == NULL && list1->first == NULL ) || ( list1->curr != NULL && list1->last != NULL && list1->first != NULL ) ) ) {
		printf( "list1 not properly formed\n" );
		return ;
	}

	if ( !( ( list2->curr == NULL && list2->last == NULL && list2->first == NULL ) || ( list2->curr != NULL && list2->last != NULL && list2->first != NULL ) ) ) {
		printf( "list2 not properly formed\n" );
		return ;
	}

	/* Link begining of list2 with the end of list1 */
	if ( list1->curr == NULL ) {//Case list1 is empty
		list1->first = list2->first;
		list1->curr = list2->first;
		list1->last = list2->last;
	}
	else if ( list2->curr != NULL ) {// Case where list1 and list2 are not empty
		list1->last->next = list2->first;
		list2->first->prev = list1->last;
		list1->last = list2->last;
	}

	// Case where list2 is empty but list1 is not, nothing to do
		/* Void */

	/* Adding the number of nodes*/
	list1->nbNode += list2->nbNode;

	/* Ressetting list varaibles ( Might be obsolete ) */
	list2->first = NULL;
	list2->last = NULL;
	list2->curr = NULL;
	list2->nbNode = 0;

	/* Make list2 available again*/
	arrayListA[( list2->locate )].nextLA = currLA;
	currLA = &arrayListA[( list2->locate )];

	return;
}
