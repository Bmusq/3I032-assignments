/* NAME:Musquer Basile, Boehm Maximilian
   NSID:bam857, mab728
   Student Number:11287646, 11288097
   Group: 34

   University of Saskatchewan
   CMPT 332 Term 1 2019
   Assignment 2
   Part B
*/


#include <os.h>
#include <standards.h>
#include <reader-writer-monitor.h>

#define SLEEPMAX 20

PID reader(void *arg)
{
  long myId;
  
  myId = (long) arg;
  
  for(;;)
    {
      printf("%ld ask to read\n", myId);
      StartRead();
      printf("%ld is reading\n", myId);
      Sleep((int) (rand() % SLEEPMAX));
      StopRead();
      printf("%ld stop read\n", myId);
      Sleep((int) (rand() % SLEEPMAX));
    }

	return myId;
}

PID writer(void *arg)
{
  long myId;
  myId = (long) arg;
  
  for(;;)
    {
      printf("%ld ask to write\n", myId);
      StartWrite();
      printf("%ld is writing\n", myId);
      Sleep((int) (rand() % SLEEPMAX*5));
      StopWrite();
      printf("%ld stop write\n", myId);
      Sleep((int) (rand() % SLEEPMAX*6));
    }
  	return myId;
}

int mainp()
{
    PID tempPid, temp2, temp3;
    setbuf(stdout, 0);

    srand(71);

	Initialize();

    tempPid = Create((void(*)()) reader, 16000, "R1", (void *) 1000, 
		      NORM, USR );
    if (tempPid == PNUL) perror("Create");
   temp2 = Create(  (void(*)()) writer, 16000, "W1", (void *) 500, 
		       NORM, USR );
    if (temp2 == PNUL) perror("Create");
    temp3 = Create(  (void(*)()) reader, 16000, "R2", (void *) 1001,
		       NORM, USR );
    if (temp3 == PNUL) perror("Create");
    temp3 = Create(  (void(*)()) reader, 16000, "R3", (void *) 1002, 
		       NORM, USR );
    if (temp3 == PNUL) perror("Create");
    temp3 = Create(  (void(*)()) writer, 16000, "W2", (void *) 501, 
		       NORM, USR );
    if (temp3 == PNUL) perror("Create");
    printf("Reader and Writer processes created\n");
    
    return(0);
}
