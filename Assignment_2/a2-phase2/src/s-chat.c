/*
Name: Basile Musquer
NSID: bam857, mab728
Student: 11287646, 11288097

group34

University of Saskatchewan
CMPT 332 Term 1 2019
Assignment 2
Part C
*/

#include <curses.h>
#include "rtthreads.h"
#include <RttThreadId.h>
#include <RttTime.h>
#include <RttCommon.h>
#include <s-chat.h>
#include <list.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#define LINE_MAX 200
#define SIZE 1024
#define _OPEN_SYS_ITOA_EXT

/* lists */
List* transit_list;
List* send_buffer;

/* Semaphores for lists */
static RttSem transitSem;
static RttSem sendSem;


/*function to read line from console*/
char* read_line(){
        int cursor=0;
        int buf_line_size=LINE_MAX;
        char *buf_line=(char *) malloc(sizeof(char)*buf_line_size);
        int c;

        if(buf_line == NULL){
                printf("Error alloc buf_line string\n");
                exit(1);
        }
        while(1){
		RttUSleep(1);
               	c=getchar();

                if(c == '\n'){
                        buf_line[cursor]='\0';
                        return buf_line;
                }
                else{
                        buf_line[cursor]=c;
                }
                cursor++;


                if(cursor>=buf_line_size){
                        buf_line_size+=LINE_MAX;
                        buf_line=realloc(buf_line, buf_line_size);

                        if(buf_line == NULL){
                                printf("Error realloc buf_line string\n");
                                exit(1);
                        }
                }

        }
}

char* new_str(char* s) {
	int len=strlen(s);
	char* new_s = (char*)malloc(sizeof(char)*len);
	int t;
	for (t = 0; t<len; t++) {
		new_s[t] = s[t];
	}
	return new_s;
}


void await_input() {

	printf("Thread >>await_input<< is running\n");	
	char *line;
	int count = 0;
	int buf_line_size=1;
    char *buf_line=(char *) malloc(sizeof(char)*buf_line_size);
	int run, i;
	char x;	

	if(buf_line == NULL){
                printf("Error alloc buf_line string\n");
                exit(1);
        }
	while(1) {
                RttUSleep(1);
		printf("Insert Message: ");
		run = 1;
		while(run) {
			line = read_line();
			if ((int)strlen(line)>3) {
				run = 0;
			}
		}
		run = 1;

		for (i = 0; i<(int)strlen(line); i++) {
			x = line[i];
			if ((int)x != -1) {
				buf_line[count] = x;
				count++;
			}
		
			if(count >= buf_line_size) {
				buf_line_size+=1;
				buf_line=realloc(buf_line, buf_line_size);

				if(buf_line == NULL){
                                	printf("Error realloc buf_line string\n");
                                	exit(1);
                        	}
			}

		}	
		if (strcmp(buf_line, "exit") == 0) {
			printf("End chat.\n");
			exit(0);
		}
		printf("MESSAGE: %s\n", buf_line);
		RttP(sendSem);	
		ListAppend(send_buffer, new_str(buf_line));
		RttV(sendSem);
		count = 0;
		free(buf_line);
		buf_line = (char *) malloc(sizeof(char)*buf_line_size);
	}
	return;
}

void *get_in_addr(struct sockaddr *sa) {
	if (sa->sa_family == AF_INET) {
		return &(((struct sockaddr_in*)sa)->sin_addr);
	}

	return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

char *my_itoa(int num, char *str)
{
        if(str == NULL)
        {
                return NULL;
        }
        sprintf(str, "%d", num);
        return str;
}

void await_datagram(Data *data) {
	if(!data->own_port || !data->con_machine || !data->con_port) {
		printf("Error: invalid parameter\n");
		exit(1);
	}

		int sockfd;
		struct addrinfo hints, *servinfo, *p;
		int rv;
		int numbytes;
		struct sockaddr_storage their_addr;
		char buf[100];
		socklen_t addr_len;
		char s[INET6_ADDRSTRLEN];
		char buffer[20];
		int flags;


		struct timeval read_timeout;
		read_timeout.tv_sec = 0;
		read_timeout.tv_usec = 100;


	printf("Thread >>await_datagram<< is running\n");
	while(1) {
		
		memset(&hints, 0, sizeof(hints));
		hints.ai_family = AF_UNSPEC;
		hints.ai_socktype = SOCK_DGRAM;
		hints.ai_flags = AI_PASSIVE;
		if ((rv = getaddrinfo(NULL, my_itoa(data->own_port,buffer), &hints, &servinfo)) != 0) {
			fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
        		return;		
		}

		for (p = servinfo; p != NULL; p = p->ai_next) {
			if ((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
				perror("listener: socket");
				continue;
			}


			if( (flags = fcntl(sockfd, F_GETFL, 0)) == -1){ flags = 0;}
			fcntl(sockfd, F_SETFL, flags | O_NONBLOCK);



			if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
				close(sockfd);
				perror("listener: bind");
				continue;
			}

			break;
		}

		if (p == NULL) {
			fprintf(stderr, "listener: failed to bind socket\n");
			return;
		}

		freeaddrinfo(servinfo);

		printf("listener: waiting to recvfrom...\n");

		addr_len = sizeof(their_addr);
		//setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, &read_timeout, sizeof(read_timeout) );

		do{
			RttUSleep(1);
			numbytes = recvfrom(sockfd, buf, 100-1, 0, (struct sockaddr*)&their_addr, &addr_len);
		}while(numbytes < 1);


		printf("listener got packet from %s\n", inet_ntop(their_addr.ss_family, 
			get_in_addr((struct sockaddr *)&their_addr), s, sizeof(s)));
	       	printf("listener: packet contains \"%s\"n", buf);
		
		close(sockfd);	

		RttP(transitSem);
		ListAppend(transit_list, new_str(buf));
		RttV(transitSem);

                RttUSleep(1);
	}
	return;
}

void print_screen() {
	printf("Thread >>print_screen<< is running\n");
	while(1) {
		if((int)ListCount(transit_list) > 0) {
			RttP(transitSem);
			printf("%s\n", (char*)ListRemove(transit_list));
			RttV(transitSem);
		}
		RttUSleep(1);
       	}
	return;
}

void send_data(Data *data) {
	if(!data->own_port || !data->con_machine || !data->con_port) {
                printf("Error: invalid parameter\n");
                exit(1);
        }

	int sockfd;
	struct addrinfo hints, *servinfo, *p;
	int rv;
	int numbytes;
	char buffer[20];
	char * send;

	printf("Thread >>send_data<< is running\n");
	while(1) {
		if((int)ListCount(send_buffer) > 0){			
			memset(&hints, 0, sizeof hints);
			hints.ai_family = AF_UNSPEC;
			hints.ai_socktype = SOCK_DGRAM;


			if ((rv = getaddrinfo(data->con_machine, 
				my_itoa(data->con_port, buffer), &hints, &servinfo)) != 0) {
				fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
        			return;
			}

			for (p = servinfo; p != NULL; p = p->ai_next) {
				if ((sockfd = socket(p->ai_family, p->ai_socktype,
					p->ai_protocol)) == -1) {
					perror("talker: socket");
            				continue;
				}

				break;
			}

			if (p == NULL) {
	     	   		fprintf(stderr, "talker: failed to create socket\n");
        			return ;
			}

			RttP(sendSem);
			send = (char*)ListRemove(send_buffer);
			RttV(sendSem);
			if ((numbytes = sendto(sockfd, send, strlen(send), 
				0, p->ai_addr, p->ai_addrlen)) ==-1) {
				perror("talker: sendto");
				exit(1);
			}

			freeaddrinfo(servinfo);

			printf("talker:sent %d bytes to %s\n", numbytes, (char*)data->con_machine);
			close(sockfd);
		}
		RttUSleep(1);
        }
	return;
}

void mainp(int argc, char *argv[]) {

	assert(argv[1] != NULL);
	assert(argv[2] != NULL);
	assert(argv[3] != NULL);

	int own_port = atoi(argv[1]);
	char* con_machine = argv[2];
	int con_port = atoi(argv[3]);

	/*Creating lists*/
	transit_list = ListCreate();
	send_buffer = ListCreate();

	/*Creating semaphores*/
	RttAllocSem(&transitSem, 1, RTTFCFS);
	RttAllocSem(&sendSem, 1, RTTFCFS);

	Data *data = malloc(sizeof(*data));
	data->own_port = own_port;
	data->con_machine = con_machine;
	data->con_port = con_port;

	if(own_port > 40000 || own_port < 30001) {
		printf("Error: invalid port on your machine\n");
		exit(1);
	}
	if(con_port > 40000 || con_port < 30001) {
		printf("Error: invalid port on connetcting machine\n");
		exit(1);
	}

	RttSchAttr attrs;
	attrs.startingtime = RTTZEROTIME;
	attrs.priority = RTTHIGH;
	attrs.deadline = RTTNODEADLINE;

	RttThreadId send, print, awaitD, awaitI;

	RttCreate(&send, send_data, 16000, "send_data_thread", 
			data, attrs, RTTUSR);
	RttCreate(&print, print_screen, 16000, "print_screen_thread", 
			data, attrs, RTTUSR);
	RttCreate(&awaitD, await_datagram, 16000, "await_datagram_thread", 
			data, attrs, RTTUSR);
	RttCreate(&awaitI, await_input, 16000, "await_input_thread", 
			data, attrs, RTTUSR);
	
	printf("threads created\n");

	//free(data);
	(void)argc;
}
