/* NAME:Musquer Basile, Boehm Maximilian
   NSID:bam857, mab728
   Student Number:11287646, 11288097
   Group: 34

   University of Saskatchewan
   CMPT 332 Term 1 2019
   Assignment 2
   Part B
*/

/************************** PRE-SETs ******************************/
/* Include */
#include <os.h>
#include <standards.h>
#include <list.h>
#include <monitor.h>

/* Server, mutex, waiting queues, message */
int msg_len=sizeof(Message);
PID server;
static int lock;
static int nCV;
List** waitingQs;
/*		[0]=entry Q	
		[1]=urgent Q
		...
		[index]= CV Q
*/

/************************** FUNCTIONS ******************************/
/******************************************************************/
/*-------------------------------*//*Initialisation*/
void MonInit(int nbQ){
	lock=0;
	int i;

	/* Create Server */	
    server = Create((void(*)()) MonServer, 16000, "server", (void *) NULL, 
		      NORM, SYS );
    if (server == PNUL){
		printf("Error in function MonInit: could not create the server");
		exit(1);
	}

	nCV=nbQ+2;
	/* Create waiting Q */
	waitingQs=(List **) malloc(sizeof(List *)*(nCV));
	for(i=0; i<nCV ; i++){
		waitingQs[i]=ListCreate();
	}
	return;
}

/*-------------------------------*//*Enter Monitor*/
void MonEnter(){
	Message msg;
	msg.signal=ENTER;
	msg.index=-1;

	Send(server, &msg, &msg_len);

	return ;
}

/*-------------------------------*//*Leave Monitor*/
void MonLeave(){
	Message msg;
	msg.signal=LEAVE;
	msg.index=-1;

	Send(server, &msg, &msg_len);

	return ;
}

/*-------------------------------*//*Wait*/
void MonWait(int indQ){
	Message msg;
	msg.signal=WAIT;
	msg.index=indQ;

	Send(server, &msg, &msg_len);

	return ;
}

/*-------------------------------*//*Signal*/
void MonSignal(int indQ){
	Message msg;
	msg.signal=SIGNAL;
	msg.index=indQ;

	Send(server, &msg, &msg_len);

	return ;
}

/*-------------------------------*//*Server*/
int MonServer(){
	Message *msg=NULL;
	PID pid;
	int len;

	printf("PID server: %ld\n", MyPid());	

	while(1){
		msg=NULL;
		msg=((Message *) Receive(&pid, &len));
		switch(msg->signal){
			case ENTER:
				if(lock){
					/* If the monitor is currently being used, put the process on the entry q*/
					if(ListPrepend(waitingQs[ENTRYQ], (void*) pid) == -1){
						printf("Error in function MonServer, case Enter: ListPrepend() failed\n");
						exit(1);
					}
				} else {
					/* Else lock the monitor and reply to the process <=> allowed him to enter*/
					lock=1;
					if(Reply(pid, NULL, 0) == -1){
						printf("Error in function MonServer, case Enter: Reply() failed\n");
						exit(1);
					}
				}
			break;
			
			case LEAVE:
				/* Reply to the sender so that he can keep executing outside the monitor */
				if(Reply(pid, NULL, 0) == -1){
					printf("Error in function MonServer, case Leave: (1)Reply() failed\n");
					exit(1);
				}

				/* Check if any process is waiting on the urgent queue*/
				if(ListCount(waitingQs[URGQ])){
					/* If yes then reply to that proces <=> resume his execution, and remove it from the queue */
					if(Reply(((PID) ListLast(waitingQs[URGQ])), NULL, 0) == -1){
						printf("Error in function MonServer, case Leave: (2)Reply() failed\n");
						exit(1);
					}
					ListTrim(waitingQs[URGQ]);
					/* If no, check if any process is waiting on the entry queue*/
				} else if(ListCount(waitingQs[ENTRYQ])) {
						/* If yes, then reply to that proces <=> resume his execution, and remove it from the queue*/
					if(Reply(((PID) ListLast(waitingQs[ENTRYQ])), NULL, 0) == -1){
						printf("Error in function MonServer, case Leave: (3)Reply() failed\n");
						exit(1);
					}
					ListTrim(waitingQs[ENTRYQ]);
						/* If no, set monitor lock to open */
				} else {
					lock=0;			
				}

			break;

			case WAIT:
				/* Check if the called waiting queue is a valid one */
				if(msg->index<0){
					printf("Error in function MonServer, case Wait: negativ index\n");
					exit(1);
				}
				if(msg->index<2){
					printf("Error in function MonServer, case Wait: index 0 and 1 are already used by the monitor. Index for condition variables start at 2\n");
					exit(1);		
				}
				if(msg->index>=nCV){
					printf("Error in function MonServer, case Wait: index OutOfBounds\n");
					exit(1);		
				}
				/* Put the process on the correspondong waiting queue*/
				if(ListPrepend(waitingQs[msg->index], (void *) pid) == -1){
					printf("Error in function MonServer, case Wait: ListPrepend() failed\n");
					exit(1);
				}
				
				/* Check if any process is waiting on the urgent queue*/
				if(ListCount(waitingQs[URGQ])){
					/* If yes then reply to that proces <=> resume his execution, and remove it from the queue */
					if(Reply(((PID) ListLast(waitingQs[URGQ])), NULL, 0) == -1){
						printf("Error in function MonServer, case Wait: (1)Reply() failed\n");
						exit(1);
					}
					ListTrim(waitingQs[URGQ]);
					/* If no, check if any process is waiting on the entry queue*/
				} else if(ListCount(waitingQs[ENTRYQ])) {
						/* If yes, then reply to that proces <=> resume his execution, and remove it from the queue*/
					if(Reply(((PID ) ListLast(waitingQs[ENTRYQ])), NULL, 0) == -1){
						printf("Error in function MonServer, case Wait: (2)Reply() failed\n");
						exit(1);
					}
					ListTrim(waitingQs[ENTRYQ]);
						/* If no, set monitor lock to open */
				} else {
					lock=0;			
				}

			break;

			case SIGNAL:
				if(msg->index<0){
					printf("Error in function MonServer, case Signal: Negativ Index\n");
					exit(1);
				}
				if(msg->index>=nCV){
					printf("Error in function MonServer, case Signal: index OutOfBounds\n");
					exit(1);		
				}
				/* Check if any process are waiting on the signaled queue */
				if(ListCount(waitingQs[msg->index])){
					/* If yes, put the process onto the urgent queue*/
					if(ListPrepend(waitingQs[URGQ], ListLast(waitingQs[msg->index])) == -1){
						printf("Error in function MonServer, case Signal: ListPrepend() failed\n");
						exit(1);
					}
					/* Remove the process from the condition variable */
					ListTrim(waitingQs[msg->index]);
				} else { /* If no, do nothing */ }

				/* Reply to the sender so that he can keep executing */
				if(Reply(pid, NULL, 0) == -1){
					printf("Error in function MonServer, case Signal: Reply() failed\n");
					exit(1);
				}
			break;

			default:
				printf("Error in function MonServer, case Default: unknown request\n");
				if(Kill(pid)==PNUL){
					printf("Error in function MonServer, case Default: thread to killed not found\n");
					exit(1);
				}
			break;
		}
	}
	return 0;
}









