/* NAME:Musquer Basile, Boehm Maximilian
   NSID:bam857, mab728
   Student Number:11287646, 11288097
   Group: 34

   University of Saskatchewan
   CMPT 332 Term 1 2019
   Assignment 2
   Part B
*/
#include <os.h>
#include <standards.h>
#include <philosopher-dinner-monitor.h>

#define SLEEPMAX 20

PID philosopher(void *arg)
{
  long myId;
  
  myId = (long) arg;
  
  for(;;)
    {
      printf("%ld ask to eat\n", myId);
      pickUp( myId);
      printf("%ld is eating\n",  myId);
      Sleep((int) (rand() % SLEEPMAX));
      putDown( myId);
      printf("%ld is thinking\n",  myId);
      Sleep((int) (rand() % SLEEPMAX));
    }

	return myId;
}

int mainp()
{
    PID tempPid, temp2, temp3;
    setbuf(stdout, 0);

    srand(71);

	Initialize();

    tempPid = Create((void(*)()) philosopher, 16000, "P1", (void *) 2, 
		      NORM, USR );
    if (tempPid == PNUL) perror("Create");
   temp2 = Create(  (void(*)()) philosopher, 16000, "P2", (void *) 3, 
		       NORM, USR );
    if (temp2 == PNUL) perror("Create");
    temp3 = Create(  (void(*)()) philosopher, 16000, "P3", (void *) 4,
		       NORM, USR );
    if (temp3 == PNUL) perror("Create");
    temp3 = Create(  (void(*)()) philosopher, 16000, "P4", (void *) 5, 
		       NORM, USR );
    if (temp3 == PNUL) perror("Create");
    temp3 = Create(  (void(*)()) philosopher, 16000, "P5", (void *) 6, 
		       NORM, USR );
    if (temp3 == PNUL) perror("Create");
    printf("Philosopher processes created\n");
    
    return(0);
}
