/* CMPT 332 GROUP 34 Change, Fall 2019 */
#include "kernel/types.h"
#include "user/user.h"

#define BUFFER_SIZE 	64
#define NPRODUCER		2
#define NCONSUMER		2
#define SZ_STACK        16384

#define MTX				0
#define FULL			1
#define EMPTY			2

char buffer[BUFFER_SIZE];
int counterP=0;
int counterC=0;
int nitems=0;
uint64 ind_mtx[3];
/*
 * ind_mtx[0]=mtx
 * ind_mtx[1]=full
 * ind_mtx[2]=empty
*/


/* Child threads */
void
consumer(void *arg){
	char c;
	int i=0;
	for(i=0;i<20;i++){
		mtx_lock(ind_mtx[FULL]);
		mtx_lock(ind_mtx[MTX]);
			if (counterC == BUFFER_SIZE) counterC = 0;
			c=buffer[counterC];
			printf("consume: %c\n", c);
			counterC ++;
			nitems --;
	  	mtx_unlock(ind_mtx[MTX]);
		mtx_unlock(ind_mtx[EMPTY]);	
	}
    (void) c;
	exit();
}

void
producer(void *arg){
  char c=32;
  int i=0;
  for(i=0;i<22;i++){
		c++;
		if(c>=127) c=33;
		mtx_lock(ind_mtx[EMPTY]);
		mtx_lock(ind_mtx[MTX]);
			if (counterP == BUFFER_SIZE) counterP = 0;
			buffer[counterP]=c;
			printf("produce: %c\n", c);
			counterP ++;
			nitems ++;
	  	mtx_unlock(ind_mtx[MTX]);	
		mtx_unlock(ind_mtx[FULL]);	
	}
	exit();
}


/* Parent thread */
int
main(int argc, char *argv[]){
	int i;
	ind_mtx[MTX]=mtx_create(1);
	ind_mtx[FULL]=mtx_create(0);
	ind_mtx[EMPTY]=mtx_create(BUFFER_SIZE);

	void * stack;
    void * stackfree;

	if (ind_mtx[MTX] == -1) {
		printf("mutex not initialized\n");
		exit();
	}
	if (ind_mtx[EMPTY] == -1) {
		printf("empty not initialized\n");
		exit();
	}

	if (ind_mtx[FULL] == -1) {
		printf("full not initialized\n");
		exit();
	}


	for(i=0; i<NCONSUMER; i++){
				if ((stack = malloc(SZ_STACK)) == 0){
                    printf("stack couldn't be allocated\n");
                    exit();
                }
                stack += SZ_STACK;
				thread_create((void(*)()) consumer, stack, (void *) 0);
	}

	for(i=0; i<NPRODUCER; i++){
				if ((stack = malloc(SZ_STACK)) == 0){
                    printf("stack couldn't be allocated\n");
                    exit();
                }
                stack += SZ_STACK;
				thread_create((void(*)()) producer, stack, (void *) 0);
	}


	for(i=0; i<NPRODUCER+NCONSUMER; i++){
		thread_join(&stackfree);
		free(stackfree-SZ_STACK);
	}
	exit();
}









