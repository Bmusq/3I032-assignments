#include "kernel/types.h"
#include "user/user.h"

#define N  10

void
forktest(void)
{
  int n;

  for(n=0; n<N; n++){
    if(fork()==0)
      while(1);
  }

  if(n == N){
    printf("fork claimed to work %d times!\n", N);
    return ;
   }
}

int
main(int argc, char *argv[])
{
  int current_runnable;

  forktest();
  current_runnable = numprocs();
		
  if (current_runnable < 0){
    printf("numprocs failed\n");
    exit();
  }
  else{
    printf("Number of runnable processes:\t%d\n", current_runnable);
  }
  exit();
}
