/* NAME:Musquer Basile, Boehm Maximilian
 * NSID:bam857, mab728
 * Student Number:11287646, 11288097
 * Group: 34
 *
 * University of Saskatchewan
 * CMPT 332 Fall 2019
 * File: LibList:list.h
*/
#ifndef _LIST_FUNC_
#define _LIST_FUNC_

/************************** PRE-SETs ******************************/
/*****************************************************************/
/* Shared include */
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

/*  Shared Define */
#define MAX_LIST 20
#define MAX_NODE 200

/* Typedef */
typedef struct _node Node;
typedef struct _node_available NodeA;
typedef struct _list List;
typedef struct _list_available ListA;

/* Node data Structure */
struct _node{
	void* item;
	Node* next;
	Node* prev;

	unsigned int pos;
};

/* Node data Structure */
struct _node_available{
	NodeA* nextNA;
	Node* ptrNode;
};

/* List data Structure */
struct _list{
	Node* curr;
	Node* first;
	Node* last;
	
	int nbNode;
	int locate;
};

/* List Available data Stucture */
struct _list_available{
	ListA* nextLA;
	List* ptrList;
};


/************************** Functions Definitions ******************************/
/******************************************************************************/

/* Create List */
List* ListCreate();

/* Count Number of items in the List*/
int ListCount(List* list);

/* Return the first item of the List*/
void* ListFirst(List* list);

/* Return the last item of the List */
void* ListLast(List* list);

/* Go to the next node and return the item of the next node*/
void* ListNext(List* list);

/* Go to the previous node and return the item of the previous node */
void* ListPrev(List* List);

/* Return the item of the current node */
void* ListCurr(List* list);

/* Add an item to the List after the current one and make it the current item */
int ListAdd(List* list, void* item);

/* Add an item to the List before the current one and make it the current item */
int ListInsert(List* list, void* item);

/* Add an item at the end of the List and make it the current item */
int ListAppend(List* list,  void* item);

/* Add an item to the front of the List and make it the current item */
int ListPrepend(List* list, void* item);

/* Return the current item and remove it of the List, make the next item the current one*/
void* ListRemove(List* list);

/* Adds List2 to the end of List1, List2* no longer exist after the operation */
void ListConcat(List* list1, List* list2);

/* Delete the List, also delete each items of each node with a function itemFree */
void ListFree(List* list, void (*itemFree)(void*));

/* Delete last item and return it, make the new last item current item */
void* ListTrim(List* list);

/* Searched an item starting at the current item till the end or till a match is found
	Match found : make the found item current item, return the item
	Match not found : make the last item current item, return NULL*/
void* ListSearch(List* list,int (*comparator)(void*, void*), void* comparisonArg);

#endif




