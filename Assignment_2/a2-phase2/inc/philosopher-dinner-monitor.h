/* NAME:Musquer Basile, Boehm Maximilian
   NSID:bam857, mab728
   Student Number:11287646, 11288097
   Group: 34

   University of Saskatchewan
   CMPT 332 Term 1 2019
   Assignment 2
   Part B
*/

#ifndef _PHILOSOPHER_DINNER_MONITOR
#define _PHILOSOPHER_DINNER_MONITOR
/************************** PRE-SETs ******************************/
/*****************************************************************/
/* Include */
#include <monitor.h>


/************************** Functions Definitions ******************************/
/******************************************************************************/

/* Initialize Monitor */
void Initialize(void);

/* Pick up forks, and thus eat if the forks are available */
void pickUp(int id);

/* Put down forks, signal neighbors in case they would be hungry */
void putDown(int id);

/* Test if the philosopher can eat or must wait */
void test(int id);

#endif
