/* NAME:Musquer Basile, Boehm Maximilian
   NSID:bam857, mab728
   Student Number:11287646, 11288097
   Group: 34

   University of Saskatchewan
   CMPT 332 Term 1 2019
   Assignment 2
   Part B
*/

/************************** PRE-SETs ******************************/
/* Include */
#include <monitor.h>
#include <os.h>
#include <standards.h>
#include <list.h>

/* Global Variables */
PID server;
static boolean lock;
List** waitingQs;
/*		[0]=entry Q	
		[1]=urgent Q
		...
		[index]= CV Q
*/

/************************** FUNCTIONS ******************************/
/******************************************************************/
/*-------------------------------*//*Initialisation*/
void MonInit(int nbQ){
	lock=FALSE;
	int i;

	/* Create Server */	
    server = Create((void(*)()) MonServer, 16000, "server", (void *) NULL, 
		      NORM, SYS );
    if (server == PNUL){
		printf("Error in function MonInit: could not create the server");
		exit(1);
	}

	/* Create waiting Q */
	waitingQs=(List **) malloc(sizeof(List *)*(nbQ+2)); // Might need to use Malloc from Pthread doc
	for(i=2; i<nbQ+2 ; i++){
		waitingQs[i]=ListCreate();
	}
	return;
}

/*-------------------------------*//*Enter Monitor*/
void MonEnter(){
	Message msg={ENTER, -1};

	if(*((int *) Send(server, &msg, (int *) sizeof(Message)))== NOSUCHPROC){
		printf("Error in function MonEnter: Send() failed\n");
		exit(1);
	}
	return ;
}

/*-------------------------------*//*Leave Monitor*/
void MonLeave(){
	Message msg={LEAVE, -1};

	if(*((int *) Send(server, &msg, (int *) sizeof(Message)))== NOSUCHPROC){
		printf("Error in function MonLeave: Send() failed\n");
		exit(1);
	}
	return ;
}

/*-------------------------------*//*Wait*/
void MonWait(int indQ){
	Message msg={WAIT, indQ};

	if(*((int *) Send(server, &msg, (int *) sizeof(Message)))== NOSUCHPROC){
		printf("Error in function MonWait: Send() failed\n");
		exit(1);
	}
	return ;
}

/*-------------------------------*//*Signal*/
void MonSignal(int indQ){
	Message msg={SIGNAL, indQ};

	if(*((int *) Send(server, &msg, (int *) sizeof(Message)))== NOSUCHPROC){
		printf("Error in function MonSignal: Send() failed\n");
		exit(1);
	}
	return ;
}

/*-------------------------------*//*Server*/
int MonServer(){
	Message *msg=NULL;
	PID *pid=NULL;
	int* len=NULL;
	
	while(1){
		msg=((Message *) Receive(pid, len));
		switch(msg->signal){
			case ENTER:
				if(lock==TRUE){
					/* If the monitor is currently being used, put the process on the entry q*/
					if(ListPrepend(waitingQs[ENTRYQ], (void *) pid) == -1){
						printf("Error in function MonServer, case Enter: ListPrepend() failed\n");
						exit(1);
					}
				} else {
					/* Else lock the monitor and reply to the process <=> allowed him to enter*/
					lock=TRUE;
					if(Reply(*pid, (void *) NULL, 0) == -1){
						printf("Error in function MonServer, case Enter: Reply() failed\n");
						exit(1);
					}
				}
			break;
			
			case LEAVE:
				/* Reply to the sender so that he can keep executing outside the monitor */
				if(Reply(*pid, (void *) NULL, 0) == -1){
					printf("Error in function MonServer, case Leave: (1)Reply() failed\n");
					exit(1);
				}
				/* Check if any process is waiting on the urgent queue*/
				if(ListCount(waitingQs[URGQ])){
					/* If yes then reply to that proces <=> resume his execution, and remove it from the queue */
					if(Reply(*((PID *) ListLast(waitingQs[URGQ])), (void *) NULL, 0) == -1){
						printf("Error in function MonServer, case Leave: (2)Reply() failed\n");
						exit(1);
					}
					ListTrim(waitingQs[URGQ]);
					/* If no, check if any process is waiting on the entry queue*/
				} else if(ListCount(waitingQs[ENTRYQ])) {
						/* If yes, then reply to that proces <=> resume his execution, and remove it from the queue*/
					if(Reply(*((PID *) ListLast(waitingQs[ENTRYQ])), (void *) NULL, 0) == -1){
						printf("Error in function MonServer, case Leave: (3)Reply() failed\n");
						exit(1);
					}
					ListTrim(waitingQs[ENTRYQ]);
						/* If no, set monitor lock to open */
				} else {
					lock=FALSE;			
				}
			break;

			case WAIT:
				/* Check if the called waiting queue is a valid one */
				if(msg->index<0){
					printf("Error in function MonServer, case Wait: invalid index\n");
					exit(1);
				}
				if(msg->index<2){
					printf("Error in function MonServer, case Wait: index 0 and 1 are already used by the monitor. Index for condition variables start at 2\n");
					exit(1);		
				}
				/* Put the process on the correspondong waiting queue*/
				if(ListPrepend(waitingQs[msg->index], (void *) pid) == -1){
					printf("Error in function MonServer, case Wait: ListPrepend() failed\n");
					exit(1);
				}
				
				/* Check if any process is waiting on the urgent queue*/
				if(ListCount(waitingQs[URGQ])){
					/* If yes then reply to that proces <=> resume his execution, and remove it from the queue */
					if(Reply(*((PID *) ListLast(waitingQs[URGQ])), (void *) NULL, 0) == -1){
						printf("Error in function MonServer, case Wait: (1)Reply() failed\n");
						exit(1);
					}
					ListTrim(waitingQs[URGQ]);
					/* If no, check if any process is waiting on the entry queue*/
				} else if(ListCount(waitingQs[ENTRYQ])) {
						/* If yes, then reply to that proces <=> resume his execution, and remove it from the queue*/
					if(Reply(*((PID *) ListLast(waitingQs[ENTRYQ])), (void *) NULL, 0) == -1){
						printf("Error in function MonServer, case Wait: (2)Reply() failed\n");
						exit(1);
					}
					ListTrim(waitingQs[ENTRYQ]);
						/* If no, set monitor lock to open */
				} else {
					lock=FALSE;			
				}

			break;

			case SIGNAL:
				/* Reply to the sender o that he can keep executing */
				if(Reply(*pid, (void *) NULL, 0) == -1){
					printf("Error in function MonServer, case Signal: Reply() failed\n");
					exit(1);
				}
				/* Check if any process are waiting on the signaled queue */
				if(ListCount(waitingQs[msg->index])){
					/* If yes, put the process onto the urgent queue*/
					if(ListPrepend(waitingQs[URGQ], ListLast(waitingQs[msg->index])) == -1){
						printf("Error in function MonServer, case Signal: ListPrepend() failed\n");
						exit(1);
					}
					/* Remove the process from the condition variable */
					ListTrim(waitingQs[msg->index]);
				} else { /* If no, do nothing */ }
			break;

			default:
				printf("Error in function MonServer, case Default: unknown request\n");
				if(Kill(*pid)==PNUL){
					printf("Error in function MonServer, case Default: thread to killed not found\n");
					exit(1);
				}
			break;
		}
	}
	return 0;
}









