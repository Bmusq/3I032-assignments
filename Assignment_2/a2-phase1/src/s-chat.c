/*
Name: Basile Musquer
NSID: bam857, mab728
Student: 11287646, 11288097

group34

University of Saskatchewan
CMPT 332 Term 1 2019
Assignment 2
Part C
*/

#include <s-chat.h>
#include "rtthreads.h"
#include <list.h>

//List* transit_list;
//List* send_buffer;

void await_input() {
	printf("Thread >>await_input<< is running\n");
	
	return;
}

void await_datagram(Data *data) {
	if(!data->own_port || !data->con_machine || !data->con_port) {
		printf("Error: invalid parameter\n");
		exit(1);
	}

	printf("Thread >>await_datagram<< is running\n");

	return;
}

void print_screen() {
	printf("Thread >>print_screen<< is running\n");
	
	return;
}

void send_data(Data *data) {
	if(!data->own_port || !data->con_machine || !data->con_port) {
                printf("Error: invalid parameter\n");
                exit(1);
        }

	printf("Thread >>send_data<< is running\n");

	return;
}

void mainp(int argc, char *argv[]) {
	
	assert(argv[1] != NULL);
	assert(argv[2] != NULL);
	assert(argv[3] != NULL);

	int own_port = atoi(argv[1]);
	char* con_machine = argv[2];
	int con_port = atoi(argv[3]);

	Data *data = malloc(sizeof(*data));
	data->own_port = own_port;
	data->con_machine = con_machine;
	data->con_port = con_port;

	if(own_port > 40000 || own_port < 30001) {
		printf("Error: invalid port on your machine\n");
		exit(1);
	}
	if(con_port > 40000 || con_port < 30001) {
		printf("Error: invalid port on connetcting machine\n");
		exit(1);
	}

	RttSchAttr attrs;
	attrs.startingtime = RTTZEROTIME;
	attrs.priority = RTTHIGH;
	attrs.deadline = RTTNODEADLINE;

	RttThreadId send, print, awaitD, awaitI;

	RttCreate(&send, send_data, 16000, "send_data_thread", 
			data, attrs, RTTUSR);
	RttCreate(&print, print_screen, 16000, "print_screen_thread", 
			data, attrs, RTTUSR);
	RttCreate(&awaitD, await_datagram, 16000, "await_datagram_thread", 
			data, attrs, RTTUSR);
	RttCreate(&awaitI, await_input, 16000, "await_input_thread", 
			data, attrs, RTTUSR);
	
	printf("threads created\n");

	//free(data);
	(void)argc;
}
