/* NAME:Musquer Basile, Boehm Maximilian
   NSID:bam857, mab728
   Student Number:11287646, 11288097
   Group: 34

   University of Saskatchewan
   CMPT 332 Term 1 2019
   Assignment 1
   Part C
*/

/************************** PRE-SETs ******************************/
/* Include */
#include <list.h>

/* Array of Node struct */
Node chunkNode_pt[MAX_NODE];

/* Array of Node available */
NodeA arrayNodeA[MAX_NODE];

/* Pointer current available node */
NodeA* currNA;

/* Init */
static unsigned int initNode=0;


/************************** FUNCTIONS ******************************/
/******************************************************************/

/*-------------------------------*//*Node Available*/
static inline Node* nodeAvailable(){
	assert(chunkNode_pt != NULL);
	Node* newNode=NULL;
	NodeA* tmp;

	/* Begining Initialisation*/
	unsigned int i=0;
	if(initNode==0){
		for(i=0; i<MAX_NODE-1; i++){
			arrayNodeA[i].ptrNode=&chunkNode_pt[i];
			chunkNode_pt[i].pos=i;
			arrayNodeA[i].nextNA=&arrayNodeA[i+1];
		}
		arrayNodeA[i].ptrNode=&chunkNode_pt[i];
		chunkNode_pt[i].pos=i;
		arrayNodeA[i].nextNA=NULL;
		currNA=arrayNodeA;		

		initNode=1;
	}

	/* Test Maximum */
	if(currNA==NULL){
		return NULL;
	}	


	/* Find available node */
	newNode=currNA->ptrNode;
	tmp=currNA;
	currNA=currNA->nextNA;
	tmp->nextNA=NULL;

	return newNode;
}

/*-------------------------------*//*List Add*/

int ListAdd(List* list, void* item){
	assert(list != NULL);
	assert(item !=NULL);
	Node* newNode=nodeAvailable();
	if(newNode==NULL) return -1;
	
	/* Case empty list */
	if(list->curr==NULL){
		list->first=newNode;
		list->last=newNode;
		newNode->prev=NULL;
		newNode->next=NULL;
	}/* Case current node is last */
	else if(list->curr->next==NULL){
		list->curr->next=newNode;
		newNode->prev=list->curr;
		newNode->next=NULL;
		list->last=newNode;
	}
	else{/* Case current node is in the middle */
		newNode->next=list->curr->next;
		list->curr->next->prev=newNode;
		list->curr->next=newNode;
		newNode->prev=list->curr;
	}

	newNode->item=item;
	list->curr=newNode;
	list->nbNode ++;

	return 0;
}

/*-------------------------------*//*List Insert*/

int ListInsert(List* list, void* item){
	assert(list != NULL);
	assert(item !=NULL);
	Node* newNode=nodeAvailable();
	if(newNode==NULL) return -1;
	
	/* Case empty list */
	if(list->curr==NULL){
		list->first=newNode;
		list->last=newNode;
		newNode->prev=NULL;
		newNode->next=NULL;
	}/* Case current node is first */
	else if(list->curr->prev==NULL){
		list->curr->prev=newNode;
		newNode->next=list->curr;
		newNode->prev=NULL;
		list->first=newNode;
	}
	else{/* Case current node is in the middle */
		newNode->next=list->curr;
		list->curr->prev->next=newNode;
		newNode->prev=list->curr->prev;
		list->curr->prev=newNode;
	}

	newNode->item=item;
	list->curr=newNode;
	list->nbNode ++;

	return 0;
}

/*-------------------------------*//*List Append*/

int ListAppend(List* list, void* item){
	assert(list != NULL);
	assert(item !=NULL);
	Node* newNode=nodeAvailable();
	if(newNode==NULL) return -1;
	
	/* Case empty list */
	if(list->curr==NULL){
		list->first=newNode;
		list->last=newNode;
		newNode->prev=NULL;
		newNode->next=NULL;
	}
	else{/* Normal case: the new node is put at the end of a non empty list*/
		list->last->next=newNode;
		newNode->prev=list->last;
		newNode->next=NULL;
		list->last=newNode;
	}

	newNode->item=item;
	list->curr=newNode;
	list->nbNode ++;

	return 0;
}

/*-------------------------------*//*List Prepend*/

int ListPrepend(List* list, void* item){
	assert(list != NULL);
	assert(item !=NULL);
	Node* newNode=nodeAvailable();
	if(newNode==NULL) return -1;
	
	/* Case empty list */
	if(list->curr==NULL){
		list->first=newNode;
		list->last=newNode;
		newNode->prev=NULL;
		newNode->next=NULL;
	}
	else{/* Normal case: the new node is put at the begining of a non empty list*/
		list->first->prev=newNode;
		newNode->next=list->first;
		newNode->prev=NULL;
		list->first=newNode;
	}

	newNode->item=item;
	list->curr=newNode;
	list->nbNode ++;

	return 0;
}

