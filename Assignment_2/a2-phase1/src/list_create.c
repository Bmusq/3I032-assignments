/* NAME:Musquer Basile, Boehm Maximilian
   NSID:bam857, mab728
   Student Number:11287646, 11288097
   Group: 34

   University of Saskatchewan
   CMPT 332 Term 1 2019
   Assignment 1
   Part C
*/

/************************** PRE-SETs ******************************/
/* Include */
#include <list.h>

/* Array of Struct List*/
List chunkList_pt[MAX_LIST];

/* Array of List_Available */
ListA arrayListA[MAX_LIST];

/* Pointer to my available list*/
ListA* currLA=NULL;

/* Init */
static unsigned int initList=0;


/************************** FUNCTIONS ******************************/
/******************************************************************/

/*-------------------------------*//*List Create*/
List* ListCreate(){
	assert(chunkList_pt !=NULL);
	List* newList=NULL;
	ListA* tmp=NULL;
	/* Begining Initialisation */
	int i=0;
	if(initList==0){
		for(i=0; i<MAX_LIST-1;i++){
			arrayListA[i].ptrList=&chunkList_pt[i];
			chunkList_pt[i].locate=i;
			arrayListA[i].nextLA=&arrayListA[i+1];
		}
		arrayListA[i].ptrList=&chunkList_pt[i];
		chunkList_pt[i].locate=i;
		arrayListA[i].nextLA=NULL;
		currLA=arrayListA;

		initList=1;
	}

	/* Test Maximum */
	if(currLA==NULL){
		printf("Maximum number of lists reach, could not create a new one\n");
		return newList;
	}

	/* Find available list */
	newList=currLA->ptrList;
	tmp=currLA;
	currLA=currLA->nextLA;
	tmp->nextLA=NULL;
	
	//Variables Initialisation
	newList->curr=NULL;
	newList->first=NULL;
	newList->last=NULL;
	newList->nbNode=0;

	return newList;
}

