/* NAME:Musquer Basile, Boehm Maximilian
   NSID:bam857, mab728
   Student Number:11287646, 11288097
   Group: 34

   University of Saskatchewan
   CMPT 332 Term 1 2019
   Assignment 1
   Part C
*/

/************************** PRE-SETs ******************************/
/* Include */
#include <list.h>


/************************** FUNCTIONS ******************************/
/******************************************************************/

/*-------------------------------*//*List Count*/
int ListCount(List* list){
	assert(list != NULL);
	return list->nbNode;
}
/*-------------------------------*//*List First*/
void* ListFirst(List* list){
	assert(list != NULL);
	list->curr=list->first;

	return list->curr->item;
}

/*-------------------------------*//*List Last*/
void* ListLast(List* list){
	assert(list != NULL);
	list->curr=list->last;
	return list->curr->item;
}

/*-------------------------------*//*List Next*/
void* ListNext(List* list){
	assert(list != NULL);
	
	if(list->curr->next==NULL)	
		return NULL;

	list->curr=list->curr->next;
	return list->curr->item;
}

/*-------------------------------*//*List Previous*/
void* ListPrev(List* list){
	assert(list != NULL);
	
	if(list->curr->prev==NULL)
		return NULL;

	list->curr=list->curr->prev;
	return list->curr->item ;
}

/*-------------------------------*//*List Current*/
void* ListCurr(List* list){
	assert(list != NULL);
	return list->curr->item;
}

/*-------------------------------*//*List Search*/
void* ListSearch(List* list,int (comparator)(void*, void*), void* comparisonArg){
	assert(list != NULL);	

	/* Searches in the list from current item*/
	while(list->curr != NULL){
		/* Try match*/
		if(comparator(list->curr->item, comparisonArg)){
			return list->curr->item;
		}
		list->curr=list->curr->next;
	}
	list->curr=list->last;
	return NULL;
}


