/* NAME:Musquer Basile, Boehm Maximilian
   NSID:bam857, mab728
   Student Number:11287646, 11288097
   Group: 34

   University of Saskatchewan
   CMPT 332 Term 1 2019
   Assignment 2
   Part B
*/

#ifndef _MONITOR
#define _MONITOR

/************************** PRE-SETs ******************************/
/*****************************************************************/
/* Include */
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

/* Define */
#define URGQ	1
#define ENTRYQ	0

#define ENTER	1
#define LEAVE	0
#define WAIT	3
#define SIGNAL	2 

/* Typedef */
typedef enum {FALSE = 0, TRUE} boolean;
typedef struct _message Message;

/* Structure */
struct _message{
	int signal;
	int index;
};

/************************** Functions Definitions ******************************/
/******************************************************************************/

/* Fake that a procedure of the monitor has been invoked */
void MonEnter();

/* Fake that the procedure of the monitor has terminated */
void MonLeave();

/* Place a process on the waiting list of the monitor */
void MonWait(int indQ);

/* Signal a waiting process that the monitor is ready to be used */
void MonSignal(int indQ);

/* Return the number of waiting processes of a queue */
int MonServer();

/* Intialize the monitor, create the array of waiting list */
void MonInit(int nbQ);

#endif
