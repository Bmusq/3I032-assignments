/* NAME:Musquer Basile, Boehm Maximilian
   NSID:bam857, mab728
   Student Number:11287646, 11288097
   Group: 34

   University of Saskatchewan
   CMPT 332 Term 1 2019
   Assignment 2
   Part C
*/

#ifndef _schat
#define _schat

//Include
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

//define data structure for threads
typedef struct {
        int own_port;
        char* con_machine;
        int con_port;
} Data;

//Function definitions

//Wait for input from keyboard
void await_input();

//Wait for a UDP datagram
void await_datagram(Data *data);

//Print to screen
void print_screen();

//Send data 
void send_data(Data *data);

#endif
