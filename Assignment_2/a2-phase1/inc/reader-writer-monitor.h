/* NAME:Musquer Basile, Boehm Maximilian
   NSID:bam857, mab728
   Student Number:11287646, 11288097
   Group: 34

   University of Saskatchewan
   CMPT 332 Term 1 2019
   Assignment 2
   Part B
*/

#ifndef _READER_WRITER_MONITOR
#define _READER_WRITER_MONITOR
/************************** PRE-SETs ******************************/
/*****************************************************************/
/* Include */
#include <monitor.h>


/************************** Functions Definitions ******************************/
/******************************************************************************/

/* Initialize Monitor */
void Initialize(void);

/* Allowed a reader to read if no writer is writting */
void StartRead();

/* Allowed a writer to write if no other writer is writing or readers reading */
void StartWrite();

/* Withdraw a reader from the monitor, signal the writer if none remains */
void StopRead();

/* Withdraw a writer from the monitor, signal a reader if any is waiting */
void StopWrite();

#endif
