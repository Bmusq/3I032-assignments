   NAME:Musquer Basile, Boehm Maximilian
   NSID:bam857, mab728
   Student Number:11287646, 11288097
   Group: 34

   University of Saskatchewan
   CMPT 332 Term 1 2019
   Assignment 2
   Part B


			/***************************************************************/
						DESIGN DOCUMENTATION PART B
			/***************************************************************/

/************************** Implementation **************************/
*********************************************************************
######################### System ###########################
The monitor itself is implemented with two set of functions:
	In file monitor.c						MonServer()
											MonWait(ind)
											MonSignal(ind)
											MonEnter()
											MonLeave()
											monInit(nb_CV)				
Note: CV stand for condition variables
	
	In file reader-writer-monitor.c			StopWrite()
											StartRead()
											StopRead()
											StartWrite()

The first file provided basic fundamental functions in order to have a 
 functional monitor structure using UBC pthreads IPC.
It means that this file can be used for any specific task monitor.

For example, in our case, the specific task the monitor has to do his managing
 reader/writer threads.
Procedures to achieve that are in the file reader-writer-monitor.c, in which 
 call to MonWait(), MonEnter(), MonSignal() and MonLeave() are made. 
It provides the functions reader threads and writer threads will call in a
 test file.

Let's assume we want a working monitor for the philosophers dinner problem.
It consists in a new specific task. In that case if we can reuse the monitor.c
 file, we have to create a philosopher-dinner-monitor.c file which contains the
 functions philosopher threads will call.
This new set of procedure will call the functions of monitor.c file. Combined,
 you end up with a monitor to manage the philosopher dinner problem.

Note: You will also need to provide a test file which create the threads and
 initialize the monitor with the correct number of condition variables the 
 problem requires



As the second set of procedures relies on a given problem, we won't explain
 much how they work. Note that they do not need to call MonServer() neither
 MonInit().
Beside that, procedures within the monitor.c file, as they can be used for
 any given problem (which involves a monitor, note that this monitor remains
 basic at all you won't be able to revive dinosaurs).

Here an explanation of how these functions work:
	In file monitor.c		MonEnter()
								Send the message ENTER to the server
	In file monitor.c		MonLeave()
								Send the message LEAVE to the server
	In file monitor.c		MonWait(ind)
								Send the message WAIT + (the index of a CV) to
																	the server
	In file monitor.c		MonSignal(ind)
								Send a signal + (the index of a CV) to the 
																	server

So far these four functions just send A messages to the server. The thread 
 calling one of these function is thus blocked, waiting for a reply from the
 server
The MonServer() function does almost all the work:
	In file monitor.c		MonServer()
								Wait to receive a message
								As soon as a message is received, it does 
								 accordingly to the message the appropriate
								 block of statements

									ENTER message:
										If the monitor is not currently being
										 used then the server sends a reply 
										 and turns the lock of the monitor 
										 to TRUE
										Else does not reply, thus the thread 
 										 is waiting, and store the pid of 
										 the thread on the entry queue list

									LEAVE message:
										Reply to the sender, so that he can
 										 keep executing outside the monitor
										Reply to a waiting thread, at first
 										 one in the urgent queue otherwise
										 one in the entry queue
										If no threads is waiting, then turns
 										 the lock of the monitor to FALSE
									SIGNAL:
										Reply to the sender so that he can keep
										 executing
										Add the pid of the first process of the
										 signaled queue on the urgent queue and
										 remove it from the signaled queue
										If the queue is empty then SIGNAL does
										 nothing
									WAIT:
										Add the pid of the sender to the
										 corresponding waiting queue, does not
										 send a reply thus the calling thread
										 is still waiting
										Reply to a waiting thread, at first one
										 in the urgent queue otherwise one in
										 the entry queue
										If no threads is waiting, then turns
										 the lock of the monitor to FALSE

From this point, you can see that these 5 functions can be used for any tasks
 or problems. 

The message is a structure of two int, the first one stand for the nature of
 the message (WAIT; LEAVE; ENTER; SIGNAL), and the second one for the index
 of a condition variables (understand waiting queue).
Thus when the message if ENTER of LEAVE, the index field is irrelevant and set
 to -1. For SIGNAL and WAIT, the index is provided as a formal parameters to
 MonWait(ind) and MonSignal(ind).


Finally the intialisation is handle by:
	In file monitor.c		MonInit(nb_CV)
								Creates the server
								Creates a list for each CV requested
								Turns the lock of the monitor to FALSE


########################## User ############################
From that point the User don't have much to do but provied two files:
 a testing file and a file containing the procedures for his specific problem
	--> reader-writer.c file
	--> reader-writer-monitor.c file

reader-writer.c file is in our case the test file, it creates threads, 
 calls MonInit(nb_CV) with the wanted number of condition variables (here 2).
That's where procedures from reader-writer-monitor.c file are called

reader-writer-monitor.c file contains the procedure of the monitor designed 
 to solve the readers writers problem. The procedures inside call the ones
 from monitor.c


/************************** Pseudo-Code monitor.c **************************/
****************************************************************************
#define ENTRYQ	0
#define URGQ	1

struct message{
	int signal;
	int index;	
}

static List ** waitQ;
static boolean lock;

void MonServer(){
	message msg
	PID pid
	int len
while(1){
	msg=Receive(pid, len)
	switch(msg.signal){
		case ENTER:
			if(lock==TRUE)
				ListPrepend(waitQ[ENTRYQ], sender_pid)
			else
				lock=TRUE
				Reply(pid, NULL, 0)
			break;

		case LEAVE:
			Reply(pid, NULL, 0)
			if waitQ[URGQ] not empty
				Reply(ListFirst(waitQ[URGQ]), NULL, 0)
				ListTrim(waitQ[URGQ])
			else if waitQ[ENTRYQ] not empty
				Reply(ListFirst(waitQ[ENTRYQ]), NULL, 0)
				ListTrim(waitQ[ENTRYQ])
			else
				lock=FALSE
			break;

		case WAIT:
			if(msg.index <2){
			print(Error in function MonServer, case WAIT: waitQ does not exist)
				exit()
			}
			ListPrepend(waitQ[msg.index], sender_pid)
			if waitQ[URGQ] not empty
				Reply(ListFirst(waitQ[URGQ]), NULL, 0)
				ListTrim(waitQ[URGQ])
			else if waitQ[ENTRYQ] not empty
				Reply(ListFirst(waitQ[ENTRYQ]), NULL, 0)
				ListTrim(waitQ[ENTRYQ])
			else
				lock=FALSE
			break;

		case SIGNAL:
			Reply(pid, NULL, 0)
			if waitQ[msg.index] not empty
				Listprepend(waitQ[URGQ], ListLast(waitQ[msg.index]))
				ListTrim(waitQ[msg.index])
			else
				do nothing
			break;
	}
}
}

MonInit(nb_CV){
	lock=FALSE
	server= CreateThread("server", MonServer(), .. )
	waitingQ[]= malloc(nb_CV)								i.e: CV
	for i from 1 to nb_CV
		waitingQ[i]=ListCreate()
}

MonWait(index_waitQ)
	Send msg{"WAIT", index_waitQ} to the server


MonSignal(index_waitQ)
	Send msg{"SIGNAL", index_waitQ} to the server


MonEnter()
	Send msg{"ENTER", -1} to the server


MonLeave()
	Send msg{"LEAVE", -1} to the server
