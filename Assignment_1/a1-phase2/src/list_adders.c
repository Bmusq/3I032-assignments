/* NAME:Musquer Basile, Boehm Maximilian
   NSID:bam857, mab728
   Student Number:11287646, 11288097
   Group: 34

   University of Saskatchewan
   CMPT 332 Term 1 2019
   Assignment 1
   Part C
*/

#include <list.h>
Node chunkNode_pt[MAX_NODE];
int countNode=0;
extern int countList;
static unsigned int initNode=0;

static inline Node* nodeAvailable(){
	assert(chunkNode_pt != NULL);
	unsigned int i=0;
	if(initNode==0){
		for(i=0; i<MAX_NODE; i++){
			(chunkNode_pt+i)->ANflag=1;
		}
		initNode=1;
	}
	
	if(countNode>=MAX_NODE){
		printf("Maximum number of nodes reach, could not create a new one\n");
		return NULL;
	}	

	for(i=0; i<MAX_NODE; i++){
		if((chunkNode_pt+i)->ANflag==1){
			(chunkNode_pt+i)->ANflag=0;
			return (chunkNode_pt+i);
		}
	}
	return NULL;
}

int ListAdd(List* list, void* item){
	assert(list != NULL);
	assert(item !=NULL);
	Node* newNode=nodeAvailable();
	if(newNode==NULL) return -1;
	
	//GESTION DATA TYPE
	if(list->curr==NULL){
		list->first=newNode;
		list->last=newNode;
		newNode->prev=NULL;
		newNode->next=NULL;
	}
	else if(list->curr->next==NULL){
		list->curr->next=newNode;
		newNode->prev=list->curr;
		newNode->next=NULL;
		list->last=newNode;
	}
	else{
		newNode->next=list->curr->next;
		list->curr->next->prev=newNode;
		list->curr->next=newNode;
		newNode->prev=list->curr;
	}

	newNode->item=item;
	list->curr=newNode;

	list->numberOfNode++;
	countNode ++;
	//printf("An item has been added to the list\n");
	return 0;
}

int ListInsert(List* list, void* item){
	assert(list != NULL);
	assert(item !=NULL);
	Node* newNode=nodeAvailable();
	if(newNode==NULL) return -1;
	
	//GESTION DATA TYPE
	if(list->curr==NULL){
		list->first=newNode;
		list->last=newNode;
		newNode->prev=NULL;
		newNode->next=NULL;
	}
	else if(list->curr->prev==NULL){
		list->curr->prev=newNode;
		newNode->next=list->curr;
		newNode->prev=NULL;
		list->first=newNode;
	}
	else{
		newNode->next=list->curr;
		list->curr->prev->next=newNode;
		newNode->prev=list->curr->prev;
		list->curr->prev=newNode;
	}

	newNode->item=item;
	list->curr=newNode;

	list->numberOfNode++;
	countNode ++;
	//printf("An item has been insert into the list\n");
	return 0;
}

int ListAppend(List* list, void* item){
	assert(list != NULL);
	assert(item !=NULL);
	Node* newNode=nodeAvailable();
	if(newNode==NULL) return -1;
	
	//GESTION DATA TYPE
	if(list->curr==NULL){
		list->first=newNode;
		list->last=newNode;
		newNode->prev=NULL;
		newNode->next=NULL;
	}
	else{
		list->last->next=newNode;
		newNode->prev=list->last;
		newNode->next=NULL;
		list->last=newNode;
	}

	newNode->item=item;
	list->curr=newNode;

	list->numberOfNode++;
	countNode ++;
	//printf("An item has been append to the list\n");
	return 0;
}

int ListPrepend(List* list, void* item){
	assert(list != NULL);
	assert(item !=NULL);
	Node* newNode=nodeAvailable();
	if(newNode==NULL) return -1;
	
	//GESTION DATA TYPE
	if(list->curr==NULL){
		list->first=newNode;
		list->last=newNode;
		newNode->prev=NULL;
		newNode->next=NULL;
	}
	else{
		list->first->prev=newNode;
		newNode->next=list->first;
		newNode->prev=NULL;
		list->first=newNode;
	}

	newNode->item=item;
	list->curr=newNode;

	list->numberOfNode++;
	countNode ++;
	//printf("An item has been prepend to the list\n");
	return 0;
}

void ListConcat(List* list1, List* list2){
	assert(list1 != NULL && list2!=NULL);
		
	//GESTION DATA TYPE
	list1->last->next=list2->first;
	list2->first->prev=list1->last;
	list1->last=list2->last;	
	list2->ALflag=1;
	countList --;
	//printf("The second list has been concatened to the first list\n");
	return;
}

