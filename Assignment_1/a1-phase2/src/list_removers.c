/* NAME:Musquer Basile, Boehm Maximilian
   NSID:bam857, mab728
   Student Number:11287646, 11288097
   Group: 34

   University of Saskatchewan
   CMPT 332 Term 1 2019
   Assignment 1
   Part C
*/

#include <list.h>
extern int countList;
extern int countNode;

void ListFree(List* list, void (*itemFree)(void*)){
	assert(list != NULL);
	printf("Got into ListFree\n");
	int* p=NULL;//Test variable
	itemFree(p);

	return ;
}

void* ListTrim(List* list){
	assert(list != NULL);
	printf("Got into ListTrim\n");

	return NULL;
}

void*  ListRemove(List* list){
	assert(list != NULL);
	printf("Got into ListRemove\n");

	return NULL;
}
