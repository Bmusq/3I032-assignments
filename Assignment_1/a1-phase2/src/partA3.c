/* NAME:Musquer Basile, Boehm Maximilian
   NSID:bam857, mab728
   Student Number:11287646, 11288097

   University of Saskatchewan
   CMPT 332 Term 1 2019
   Assignment 1
   Part A3
*/

#include <square.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <pthread.h>
#include <unistd.h>

/*
//Data structure for threads
typedef struct MyData {
       	int val1;
	int val2;
} MYDATA, *PMYDATA;
*/

/*
int *counters;
int *times;
*/
int nbThread=0;

//to store current status of thread
int *running;

void *childMain(void *size) {
	
	printf("In child thread\n");

	/*
	FILETIME st, et;
	ULONGLONG stNS, etNS;

	GetSystemTimeAsFileTime(&st);
	stNS = (((ULONGLONG) st.dwHighDateTime) << 32) + st.dwLowDateTime;
	*/
	int counter =nbThread++;

	int square_size=*((int*)size);

	//check parameters
	if(square_size<= 0) {
               fprintf(stderr, "Invalid size!");
               exit(1);
        }

	for(int i=1; i<=square_size;i++){
		Square(i);	
		//counters[counter]++;
		
		if(i==square_size) {
			/*
			GetSystemTimeAsFileTime(&et);
			etNS = (((ULONGLONG) et.dwHighDateTime) << 32) + et.dwLowDateTime;
			long long totalT = (etNS-stNS);
			times[counter]= totalT;
			printf("Running time: %I64i\n", totalT);
			*/
			printf("Child thread finished\n");
			running[counter]=1;
			pthread_exit(NULL);
		}
	}

	return 0;
}

//create threads and call child_main for every created thread
int parentMain(int thread, int deadline, int size) {

       	//check thread parameter
	if(thread <= 0) {
        	fprintf(stderr, "Invalid number of threads!");
        	exit(1);
	}

	if(deadline <= 0) {
        	fprintf(stderr, "Invalid deadline!");
        	exit(1);
	}

	printf("Parameters checked\n");
		
	pthread_t threads[thread];
	int rc;
	
	//FILETIME thread_times[thread];

	printf("Creating threads\n");

	for(int i=0;i<thread;i++) {
		rc=pthread_create(&threads[i], NULL, childMain, &size);
		//GetSystemTimeAsFileTime(&thread_times[i]);
		if (rc) {
			printf("ERROR; return code from pthread_create() is %d\n", rc);
			exit(-1);
		}
		printf("Thread: %d",i);
		printf(" created\n");
	}
	printf("Threads created succesfully\n");
	
	sleep(deadline);
	
	for(int i=0;i<thread;i++) {
		/*
		FILETIME endTime;
		ULONGLONG stNS, etNS;
		GetSystemTimeAsFileTime(&endTime);
		stNS = (((ULONGLONG) thread_times[i].dwHighDateTime) << 32) + thread_times[i].dwLowDateTime;
		etNS = (((ULONGLONG) endTime.dwHighDateTime) << 32) + endTime.dwLowDateTime;
		long long totalT = (etNS-stNS);
		if(times[i]==0) {
				printf("Running time: %I64i\n", totalT);
		}
		*/	
		if(running[i]==0) {
			pthread_cancel(threads[i]);
			printf("Child thread killed\n");
		}
	}
	/*
	for(int j=0;j<thread;j++) {
			printf("Invocations of square: %d\n", counters[j]);
		}
	*/
	
        return 0;
}


//get commandline parameters and call parentMain
int main(int argc, char *argv[]){

	assert(argv[1] != NULL);
	assert(argv[2] != NULL);
	assert(argv[3] != NULL);

	int thread = atoi(argv[1]);
	int deadline = 	atoi(argv[2]);
	int size = atoi(argv[3]);

	/*
	counters= (int *) malloc(thread*sizeof(int));
	times = (int *) malloc(thread*sizeof(int));
	int i;
	for(i=0; i<thread;i++){
			times[i]=0;
			counters[i]=0;
	}
	*/

	running= (int *) malloc(thread*sizeof(int));
	int i = 0;
	for(i=0; i<thread;i++) {
		running[i]=0;
	}

	printf("In parent thread\n");

	parentMain(thread, deadline, size);
      	
	printf("Programm finished\n");

        return 0;
}
