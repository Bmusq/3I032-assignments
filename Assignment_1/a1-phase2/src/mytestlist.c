/* NAME:Musquer Basile, Boehm Maximilian
   NSID:bam857, mab728
   Student Number:11287646, 11288097
   Group: 34

   University of Saskatchewan
   CMPT 332 Term 1 2019
   Assignment 1
   Part C
*/

#include <list.h>

#define CAST1 (double *)

extern int countList;
extern int countNode;

int comparator(void* a, void* b){
	printf("Got into comparator\n");
	return 0;
}

void itemFree(void* item){
	printf("Got into itemFree\n");
	return ;
}

int addItem(void* item[MAX_NODE], int * nbItem, void* value){
	unsigned int i;
	for(i=0; i<MAX_NODE; i++){
		if(item[i]==NULL){
			item[i]=value;
			(*nbItem)++;
			return 0;
		}
	}
	return -1;
}

int addList(List *array[MAX_LIST], int * nbList){
	unsigned int i=0;
	for(i=0; i<MAX_LIST; i++){
		if(array[i]==NULL){
			array[i]=ListCreate();
			(*nbList) ++;
			return 0;
		}
	}
	return 1;
}

void displayItem(void* item[MAX_NODE], FILE *f){
	unsigned int i;
	fprintf(f, "Item in the array of items of the users :\n\t");
	for(i=0; i<MAX_NODE; i++){
		if(item[i]==NULL)
			break;
		else
			fprintf(f, "%.2f\t", *(CAST1(item[i])));
	}
	fprintf(f, "\n");
	return ;
}

int main(int argc, char *arv[]){
	List* arrayList[MAX_LIST]={NULL};
	void* item[MAX_NODE]={NULL};
	FILE *f;
	f=fopen("./documentation/PartC.testresults.txt", "w");
	int nbItem=0;
	int nbList=0;

/*******************General Test*******************/
/*************************************************/
	fprintf(f, "/*******************General Test***********************/\n");		
	//Define ITEM
	double x1=1, x2=2, x3=3, x4=4, x5=5;
	addItem(item, &nbItem, &x1);
	addItem(item, &nbItem, &x2);
	addItem(item, &nbItem, &x3);
	addItem(item, &nbItem, &x4);
	addItem(item, &nbItem, &x5);

	//DISPLAY ITEM	
	displayItem(item, f);

	// ADD LIST
	fprintf(f, "\nCreating two list:\n\t");
	addList(arrayList, &nbList);
	fprintf(f, "\t");
	addList(arrayList, &nbList);
	
	//DISPLAY LIST
	fprintf(f, "Number of list : %d\n\n", countList);	

	//ADD NODE INTO THE LIST
	ListAdd(arrayList[0], item[0]);
	fprintf(f, "Adding %.2f in the list1 (empty)\n", *(CAST1(item[0])));

	ListInsert(arrayList[0], item[1]);
	fprintf(f, "Insert %.2f in the list1 (before %.2f)\n", *(CAST1(item[1])), *(CAST1(item[0])));

	ListAppend(arrayList[0], item[2]);
	fprintf(f, "Append %.2f at the end of the list1\n", *(CAST1(item[2])));

	ListPrepend(arrayList[0], item[3]);
	fprintf(f, "Prepend %.2f at the end of the list1\n", *(CAST1(item[3])));


	fprintf(f, "Order of item in the list1 should be: %.2f %.2f %.2f %.2f\n", *(CAST1(item[3])), *(CAST1(item[1])), *(CAST1(item[0])), *(CAST1(item[2])));

	//DISPLAY THE LINK BETWEEN NODES
	fprintf(f, "\nDisplaying link betwwen nodes of list1\n");
	Node* nodeDisplay=arrayList[0]->first;
	while(nodeDisplay !=NULL){
		fprintf(f, "Previous : %p\t", nodeDisplay->prev);
		fprintf(f, "Cuurent : %p\t", nodeDisplay);
		fprintf(f, "Next : %p\n", nodeDisplay->next);
		nodeDisplay=nodeDisplay->next;
	}
	fprintf(f, "\n");

	//DISPLAY CONTENT OF NODES
	nodeDisplay=arrayList[0]->first;
	fprintf(f, "\nDisplaying content of list 1's nodes:\n");
	while(nodeDisplay !=NULL){
		if(nodeDisplay->prev !=NULL)
			fprintf(f, "Previous : %.2f\t\t", *(CAST1(nodeDisplay->prev->item)));
		fprintf(f, "Current : %.2f\t\t", *(CAST1(nodeDisplay->item)));
		if(nodeDisplay->next !=NULL)
			fprintf(f, "Next : %.2f\t\t", *(CAST1(nodeDisplay->next->item)));

		fprintf(f, "\n");
		nodeDisplay=nodeDisplay->next;
	}
	fprintf(f, "\n");

	//ADD NODE TO LIST2 ; DISPLAY NUMBER OF NODE
	fprintf(f, "Number of node : %d\n", countNode);
	fprintf(f, "Inserting an item in list 2(empty)\n");
	ListInsert(arrayList[1], item[4]);
	fprintf(f, "Number of node : %d\n\n", countNode);

	//CONCAT LIST
	fprintf(f, "Concat");
	ListConcat(arrayList[0], arrayList[1]);
	arrayList[1]=NULL;
	nbList --;

	//DISPLAY REMAINING LIST
	if(arrayList[1] == NULL)
		fprintf(f, "\tList deleted\n");
	fprintf(f, "Number of list : %d\n", countList);



/*******************Test Cases*******************/
/***********************************************/
	fprintf(f, "\n\n/*******************Test Cases***********************/\n");	
	int y1=1, y2=2, y3=3, y4=4, y5=5;
	unsigned int i;
	addItem(item, &nbItem, &y1);
	addItem(item, &nbItem, &y2);
	addItem(item, &nbItem, &y3);
	addItem(item, &nbItem, &y4);

	fprintf(f, "\nCreating two new lists\n");
	addList(arrayList, &nbList);
	addList(arrayList, &nbList);
	fprintf(f, "\nAppending an item in empty list 3\n");
	ListAppend(arrayList[1], item[5]);


	fprintf(f, "\nPrepending an item in empty list 4\n");
	ListPrepend(arrayList[2], item[6]);
	
	fprintf(f, "\nAdding an item at the end of list 3\n");
	ListAdd(arrayList[1], item[7]);

	fprintf(f, "\nInserting an item in front of list 4\n");
	ListAdd(arrayList[2], item[8]);
	
	fprintf(f, "\nExceeding max number of lists: \n");
	for(i=0; i<6; i++){
		if(addList(arrayList, &nbList))
			fprintf(f, "\nMax number of list reached\n");
		else{
			fprintf(f, "%d/%d\t", nbList, MAX_LIST);
		}
	}

	fprintf(f, "\nExceeding max number of items: \n");
	for(i=0; i<40; i++){
		if(addItem(item, &nbItem, &y5))
			fprintf(f, "\nMax number of node reached\n");
		else{
			fprintf(f, "%d/%d\t", nbItem, MAX_NODE);
		}
		if(!(i%10))
			fprintf(f, "\n");
	}



/*******************Extra Test*******************/
/***********************************************/
	//fprintf(f, "\n\n/*******************Extra Test***********************/\n");
	ListCount(arrayList[0]);
	ListFirst(arrayList[0]);
	ListLast(arrayList[0]);
	ListNext(arrayList[0]);
	ListPrev(arrayList[0]);
	ListCurr(arrayList[0]);
	ListSearch(arrayList[0], comparator, item[0]);
	ListTrim(arrayList[0]);
	ListFree(arrayList[0], itemFree);
	ListRemove(arrayList[0]);


	fprintf(f, "\nEND OF PROGRAM NO INTERRUPTION\n");
	fclose(f);
	return 0;
}
