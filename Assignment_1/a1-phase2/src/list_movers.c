/* NAME:Musquer Basile, Boehm Maximilian
   NSID:bam857, mab728
   Student Number:11287646, 11288097
   Group: 34

   University of Saskatchewan
   CMPT 332 Term 1 2019
   Assignment 1
   Part C
*/

#include <list.h>

int ListCount(List* list){
	assert(list != NULL);
	printf("Got into ListCount\n");
	return 0;
}

void* ListFirst(List* list){
	assert(list != NULL);
	printf("Got into ListFirst\n");
	return NULL;
}


void* ListLast(List* list){
	assert(list != NULL);
	printf("Got into ListLast\n");
	return NULL;
}


void* ListNext(List* list){
	assert(list != NULL);
	printf("Got into ListNext\n");
	return NULL;
}


void* ListPrev(List* list){
	assert(list != NULL);
	printf("Got into ListPrev\n");
	return NULL;
}


void* ListCurr(List* list){
	assert(list != NULL);
	printf("Got into ListCurr\n");
	return NULL;
}


void* ListSearch(List* list,int (comparator)(void*, void*), void* comparisonArg){
	assert(list != NULL);
	printf("Got into ListSearch\n");
	//TEST VARIABLES
	int* p1=NULL;
	int* p2=NULL;
	comparator(p1,p2);

	return NULL;
}


