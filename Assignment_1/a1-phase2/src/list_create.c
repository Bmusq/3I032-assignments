/* NAME:Musquer Basile, Boehm Maximilian
   NSID:bam857, mab728
   Student Number:11287646, 11288097
   Group: 34

   University of Saskatchewan
   CMPT 332 Term 1 2019
   Assignment 1
   Part C
*/

#include <list.h>
List chunkList_pt[MAX_LIST];
int countList=0;
static unsigned int initList=0;

List* ListCreate(){
	assert(chunkList_pt !=NULL);

	unsigned int i=0;
	if(initList==0){
		for(i=0; i<MAX_LIST; i++){
			(chunkList_pt+i)->ALflag=1;
		}
		initList=1;
	}

	List* newList=NULL;
	if(countList>=MAX_LIST){
		printf("Maximum number of lists reach, could not create a new one\n");
		return newList;
	}
	

	for(i=0; i<MAX_LIST;i++){
		if((chunkList_pt+i)->ALflag == 1){
			newList=chunkList_pt+i;
			break;
		}
	}

	newList->curr=NULL;
	newList->first=NULL;
	newList->last=NULL;
	newList->numberOfNode=0;
	newList->ALflag=0;

	countList ++;
	//printf("A new List has been created\n");
	return newList;
}

