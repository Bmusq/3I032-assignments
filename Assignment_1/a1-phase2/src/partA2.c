/* NAME:Musquer Basile, Boehm Maximilian
   NSID:bam857, mab728
   Student Number:11287646, 11288097

   University of Saskatchewan
   CMPT 332 Term 1 2019
   Assignment 1
   Part A2
*/

#include <square.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <standards.h>
#include <os.h>

/*
//Data structure for threads
typedef struct MyData {
       	int val1;
		int val2;
} MYDATA, *PMYDATA;

int *counters;
int *times;
int nbThread=0;
*/

PID *threads;

PROCESS childMain(int size) {
	
	printf("Got to childMain");
	
	//Time measurement form windows threads (will be changed)
	/*
	FILETIME st, et;
	ULONGLONG stNS, etNS;

	GetSystemTimeAsFileTime(&st);
	stNS = (((ULONGLONG) st.dwHighDateTime) << 32) + st.dwLowDateTime;

	int square_size=*((int*)size);
	int counter =nbThread++;
	
	//check parameters
	if(square_size<= 0) {
               fprintf(stderr, "Invalid size!");
               exit(1);
        }

	for(int i=1; i<=square_size;i++){
		Square(i);	
		counters[counter]++;
		
		if(i==square_size) {
			GetSystemTimeAsFileTime(&et);
			
			etNS = (((ULONGLONG) et.dwHighDateTime) << 32) + et.dwLowDateTime;
			long long totalT = (etNS-stNS);
			times[counter]= totalT;
			printf("Running time: %I64i\n", totalT);
			pthread_exit(NULL);
		}
	}
	*/
}

//create threads and call child_main for every created thread
int parentMain(int thread, int deadline, int size) {

        //check thread parameter
    if(thread <= 0) {
        fprintf(stderr, "Invalid number of threads!");
        exit(1);
    }
	if(deadline <= 0) {
        fprintf(stderr, "Invalid deadline!");
        exit(1);
    }

	printf("Checked parameters\n");
	
	int i;	
	for (i = 0; i < thread; i++) {
		threads[i] = Create( ( void(*)() ) childMain, 16000, "childMain", &size, NORM, USR );
		printf("PID of thread: %d\n", threads[i]);
    	}

	for (i=0;i<thread;i++) {
		printf("Thread:  %d", i);
		printf(" exists? (0=false/1=true): %d\n", PExists(i));
	}

	printf("Threads created");
	
	//Sleep(deadline);
	
	/*	
	for (i=0; i<thread;i++) {
		Kill(i);
		printf("Thread: ");
		printf("%d", i);
		printf(" killed");	
	}
	*/	

	/*	
	pthread_t threads[thread];
	int rc;
	FILETIME thread_times[thread];

	for(int i=0;i<thread;i++) {
		rc=pthread_create(&threads[i], NULL, childMain, &size);
		GetSystemTimeAsFileTime(&thread_times[i]);
		if (rc) {
			printf("ERROR; return code from pthread_create() is %d\n", rc);
			exit(-1);
		}
	}
	
	Sleep(deadline*1000);
	
	for(int i=0;i<thread;i++) {
		FILETIME endTime;
		ULONGLONG stNS, etNS;
		GetSystemTimeAsFileTime(&endTime);
		stNS = (((ULONGLONG) thread_times[i].dwHighDateTime) << 32) + thread_times[i].dwLowDateTime;
		etNS = (((ULONGLONG) endTime.dwHighDateTime) << 32) + endTime.dwLowDateTime;
		long long totalT = (etNS-stNS);
		if(times[i]==0) {
				printf("Running time: %I64i\n", totalT);
		}
			
		pthread_kill(threads[i],0);
	}
	
	for(int j=0;j<thread;j++) {
			printf("Invocations of square: %d\n", counters[j]);
		}
		*/
        return 0;
}


//get commandline parameters and call parentMain
int main(int argc, char *argv[]){

	assert(argv[1] != NULL);
    assert(argv[2] != NULL);
    assert(argv[3] != NULL);

	int thread = atoi(argv[1]);
	int deadline = 	atoi(argv[2]);
	int size = atoi(argv[3]);

	/*
	counters= (int *) malloc(thread*sizeof(int));
	times = (int *) malloc(thread*sizeof(int));
	int i;
	for(i=0; i<thread;i++){
			times[i]=0;
			counters[i]=0;
	}
	*/

	threads = (int *) malloc(thread*sizeof(int));

	printf("In parent thread\n");

	parentMain(thread, deadline, size);
      
        return 0;
}
