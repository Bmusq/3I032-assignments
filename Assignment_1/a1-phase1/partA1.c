/* NAME:Musquer Basile, Boehm Maximilian
   NSID:bam857, mab728
   Student Number:11287646, 11288097

   University of Saskatchewan
   CMPT 332 Term 1 2019
   Assignment 1
   Part A1
*/

#include "square.h"
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int childMain(int size) {

	int squareReturn = -1;

	printf("Got to child_main\n");
	
	//check parameters
	if(size <= 0) {
               fprintf(stderr, "Invalid deadline!");
                exit(1);
        }

	squareReturn = Square(size);
	
	assert(squareReturn != -1);

	if(squareReturn < 0) {
		fprintf(stderr, "Invalid return value of square function!");
		exit(1);
	}	

	printf("Result: ");
	printf("%d", squareReturn);
	printf(" \n");	
	printf(" \n");

	return 0;
}

//create threads and call child_main for every created thread
int parentMain(int thread, int deadline, int size) {

        printf("Got to parent_mai\n");

        //check thread parameter
        if(thread <= 0) {
                fprintf(stderr, "Invalid number of threads!");
                exit(1);
        } else if(deadline <= 0) {
               fprintf(stderr, "Invalid deadline!");
               exit(1);
        }

	//Threads will be stored in an array

	//Threads will be created here
	
	//gets called for every single thread
        childMain(size);

	

        return 0;
}


//get commandline parameters and call parentMain
int main(int argc, char *argv[]){

	assert(argv[1] != NULL);
        assert(argv[2] != NULL);
        assert(argv[3] != NULL);

	int thread = atoi(argv[1]);
	int deadline = 	atoi(argv[2]);
	int size = atoi(argv[3]);

	parentMain(thread, deadline, size);
      
        return 0;
}
