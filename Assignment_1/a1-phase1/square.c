/* NAME:Musquer Basile, Boehm Maximilian
   NSID:bam857, mab728
   Student Number:11287646, 11288097
   Group: 34

   University of Saskatchewan
   CMPT 332 Term 1 2019
   Assignment 1
   Part A1
*/


#include <stdio.h>

int Square(int n)
{
	if(n == 0) { 
		printf("Got to square function\n");
		return (0);
	}
	return (Square(n-1) + n + n - 1);
}

