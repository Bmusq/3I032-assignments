/* NAME:Musquer Basile, Boehm Maximilian
   NSID:bam857, mab728
   Student Number:11287646, 11288097
   Group: 34

   University of Saskatchewan
   CMPT 332 Term 1 2019
   Assignment 1
   Part C
*/

#include "list.h"
extern List* chunkList_pt;
extern Node* chunkNode_pt;

int comparator(void* a, void* b){
	printf("Got into comparator\n");
	return 0;
}

void itemFree(void* item){
	printf("Got into itemFree\n");
	return ;
}


int main(int argc, char *arv[]){
	int* p=NULL;
	chunkList_pt=(List*) malloc(sizeof(List)*MIN_SIZE_MEMORY_CHUNK_LIST);
	chunkNode_pt=(Node*) malloc(MIN_SIZE_MEMORY_CHUNK_NODE*sizeof(Node));

	ListCreate();
	ListCount(chunkList_pt);
	ListFirst(chunkList_pt);
	ListLast(chunkList_pt);
	ListNext(chunkList_pt);
	ListPrev(chunkList_pt);
	ListCurr(chunkList_pt);
	ListSearch(chunkList_pt, comparator, p);
	ListTrim(chunkList_pt);
	ListFree(chunkList_pt, itemFree);
	ListAdd(chunkList_pt, p);
	ListInsert(chunkList_pt, p);
	ListAppend(chunkList_pt, p);
	ListPrepend(chunkList_pt, p);
	ListConcat(chunkList_pt, chunkList_pt);

	return 0;
}

