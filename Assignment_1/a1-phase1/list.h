/* NAME:Musquer Basile, Boehm Maximilian
   NSID:bam857, mab728
   Student Number:11287646, 11288097
   Group: 34

   University of Saskatchewan
   CMPT 332 Term 1 2019
   Assignment 1
   Part C
*/

#ifndef _LIST_FUNC_
#define _LIST_FUNC_

/* Shared include */
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

/*  Shared Define */
#define MIN_SIZE_MEMORY_CHUNK_LIST 2
#define MIN_SIZE_MEMORY_CHUNK_NODE 6

#define MAX_SIZE_MEMORY_CHUNK_LIST 8
#define MAX_SIZE_MEMORY_CHUNK_NODE 48

/* TYPEDEF */
typedef struct _node Node;
typedef struct _list List;

/* Node data Structure */
struct _node{
	void* item;
	Node* next;
	Node* prev;
};

/* List data Structure */
struct _list{
	Node* curr;
	Node* first;
	Node* last;

	int numberOfNode;
	char *data_type;
};

/* Create List */
List* ListCreate();

/* Count Number of items in the List*/
int ListCount(List* list);

/* Return the first item of the List*/
void* ListFirst(List* list);

/* Return the last item of the List */
void* ListLast(List* list);

/* Go to the next node and return the item of the next node*/
void* ListNext(List* list);

/* Go to the previous node and return the item of the previous node */
void* ListPrev(List* List);

/* Return the item of the current node */
void* ListCurr(List* list);

/* Add an item to the List after the current one and make it the current item */
int ListAdd(List* list, void* item);

/* Add an item to the List before the current one and make it the current item */
int ListInsert(List* list, void* item);

/* Add an item at the end of the List and make it the current item */
int ListAppend(List* list,  void* item);

/* Add an item to the front of the List and make it the current item */
int ListPrepend(List* list, void* item);

/* Return the current item and remove it of the List, make the next item the current one*/
void* ListRemove(List* list);

/* Adds List2 to the end of List1, List2* no longer exist after the operation */
void ListConcat(List* list1, List* list2);

/* Delete the List, also delete each items of each node with a function itemFree */
void ListFree(List* list, void (*itemFree)(void*));

/* Delete last item and return it, make the new last item current item */
void* ListTrim(List* list);

/* Searched an item starting at the current item till the end or till a match is found
	Match found : make the found item current item, return the item
	Match not found : make the last item current item, return NULL*/
void* ListSearch(List* list,int (*comparator)(void*, void*), void* comparisonArg);

#endif




