   NAME:Musquer Basile, Boehm Maximilian
   NSID:bam857, mab728
   Student Number:11287646, 11288097
   Group: 34

   University of Saskatchewan
   CMPT 332 Term 1 2019
   Assignment 1
   Part D

----------------------------------------------------------------------------
In folder xv6-riscv/user:
	New file test_numprocs.c:
		#include "kernel/types.h"
		#include "user/user.h"

		#define N  10

		void
		forktest(void)
		{
		  int n;

		  for(n=0; n<N; n++){
		    if(fork()==0)
		      while(1);
		  }

		  if(n == N){
		    printf("fork claimed to work %d times!\n", N);
		    return ;
		   }
		}

		int
		main(int argc, char *argv[])
		{
		  int current_runnable;

		  forktest();
		  current_runnable = numprocs();
				
		  if (current_runnable < 0){
		    printf("numprocs failed\n");
		    exit();
		  }
		  else{
		    printf("Number of runnable processes:\t%d\n", current_runnable);
		  }
		  exit();
		}

	In file user.h:
		int numprocs(void);

	In file usys.pl:
		entry("numprocs");

In root folder:
	In file Makefile
		line:135	$U/_test_numprocs\

	Note: See design doc for more explanation in section Makefile Explanation

In folder xv6-riscv/kernel:
	In file defs.h:
		int		numprocs(void);

	In file sysproc.c:
		uint64
		sys_numprocs(void)
		{
		  return numprocs();
		}

	In file syscall.c:
		extern uint64 sys_numprocs(void);
		
		/*static uint64 (*syscalls[])(void) = {
			[. . .]
		[SYS_numprocs]sys_numprocs,				Statement I add
		/*};

	In file syscall.h:
		#define SYS_numprocs	22

	In file proc.c:
		int
		numprocs(void)
		{
		  uint xnumprocs=0;
		  struct proc *p;

		  for(p = proc; p < &proc[NPROC]; p++){
		    acquire(&p->lock);
		    if (p == ((void *)0)){
		      return -1;
		    }
		    if(p->state == RUNNABLE) {
		      xnumprocs ++;
		    }
		    release(&p->lock);
		  }
		  return xnumprocs;
	
	}

