/* NAME:Musquer Basile, Boehm Maximilian
   NSID:bam857, mab728
   Student Number:11287646, 11288097

   University of Saskatchewan
   CMPT 332 Term 1 2019
   Assignment 1
   Part A1
*/

#include <square.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <windows.h>
#include <process.h>
#include <tchar.h>
#include <strsafe.h>
#include <stdbool.h>

//Data structure for threads
typedef struct MyData {
       	int val1;
} MYDATA, *PMYDATA;

//Variable for threads, to check if they should exit
bool keep_running=TRUE;

DWORD WINAPI childMain(LPVOID lpParam) {;
	 
	int square_size=*((int*)lpParam);
	 
	//check parameters
	if(square_size<= 0) {
               fprintf(stderr, "Invalid size!");
               exit(1);
        }
	 
    FILETIME st, et;
	ULONGLONG stNS, etNS;

	GetSystemTimeAsFileTime(&st);
	stNS = (((ULONGLONG) st.dwHighDateTime) << 32) + st.dwLowDateTime;

	int counter = 0;
	int i;

	for(i=1; i<=square_size;i++){
		if(keep_running) {
		Square(i);	
		counter++;
		}
		if(!keep_running || i==square_size){
			GetSystemTimeAsFileTime(&et);
			etNS = (((ULONGLONG) et.dwHighDateTime) << 32) + et.dwLowDateTime;
			long long totalT = (etNS-stNS);
			printf("Running time: %I64i\n", totalT);
			printf("Invocationis of square: %d\n", counter);
			ExitThread(0);
		}
	}

	return 0;
}

//create threads and call child_main for every created thread
int parentMain(int thread, int deadline, int size) {

        //check thread parameter
        if(thread <= 0) {
                fprintf(stderr, "Invalid number of threads!");
                exit(1);
        } else if(deadline <= 0) {
               fprintf(stderr, "Invalid deadline!");
               exit(1);
        }

	//Arrays to store threads and to store thread parameters
	PMYDATA pDataArray[thread];
	HANDLE hThreadArray[thread];

	//creating threads and calling childMain()
	for(int i=0; i<thread; i++) {
		pDataArray[i] = (PMYDATA) HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, sizeof(MYDATA));

		if(pDataArray[i] == NULL) {
			ExitProcess(2);
		}

		pDataArray[i]->val1 = size;

		hThreadArray[i] = CreateThread(
			NULL,
			0,
			childMain,
			pDataArray[i],
			0,
			NULL);

		if(hThreadArray[i] == NULL) {
			fprintf(stderr, "Thread is NULL");
			ExitProcess(3);
		}
	}

	Sleep(deadline*1000);

	keep_running=FALSE;
	
        return 0;
}


//get commandline parameters and call parentMain
int main(int argc, char *argv[]){

	assert(argv[1] != NULL);
    assert(argv[2] != NULL);
    assert(argv[3] != NULL);

	int thread = atoi(argv[1]);
	int deadline = 	atoi(argv[2]);
	int size = atoi(argv[3]);

	parentMain(thread, deadline, size);
      
    return 0;
}
