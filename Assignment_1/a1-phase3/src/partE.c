/* NAME:Musquer Basile, Boehm Maximilian
   NSID:bam857, mab728
   Student Number:11287646, 11288097

   University of Saskatchewan
   CMPT 332 Term 1 2019
   Assignment 1
   Part E
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <readline/readline.h>
#include <readline/history.h>

#define MAXCOM 1000
#define MAXLIST 100

#define clear() printf("\033[H\033[J")

//standard locations
char **standard_locations_path;
char **standard_locations_home;

//to check if child is running
int wait_child = 0;

//welcome to user
void start_shell(void)
{
	clear();
	printf("\n\n\n***********************NEW SHELL**********************\n\n\n");
}

//print current directory
void printDirPrompt(void)
{
	char cwd[1024];
	getcwd(cwd, sizeof(cwd));
	printf("%s", cwd);
}

//read user input
char* getInput(char* str)
{
	char* input;

	input = readline(">>> ");	
	if (!input) {
		fprintf(stderr, "Input error!\n");
		exit(1);
	} else if (input[0] == '\0'){
		return "";
	} else {
		add_history(input);
		strcpy(str, input);
	}

	return str;

}

//tokenize input
char **tokenize(char *str, char *dil)
{
	char **token_holder;
	char *token;
	unsigned int length;
	int i;
	
	str[strlen(str)] = '\0';
	length = strlen(str);
	if (length == 0)
		return (NULL);

	token_holder = (char **) malloc((sizeof(char *)) * (length +1));
	if (token_holder == NULL)
		return (NULL);
	i = 0;

	token = strtok(str, dil);
	while (token != NULL)
	{
		token_holder[i] = (char *) malloc(strlen(token) + 1);
		if (token_holder[i] == NULL)
		{	
			int i;
			for (i=0; i<sizeof(token_holder); i++)
			{
				free(token_holder[i]);
			}
			free(token_holder);
			return (NULL);
		}
		strncpy(token_holder[i], token, strlen(token) + 1);
		token = strtok(NULL, dil);
		++i;
	}
	token_holder[i] = NULL;
	return (token_holder);
}

//Declaration of builtin commands
int cd_builtin(char **args);
int exit_builtin(char **args);

//store commands for user-input
char *builtin_str[] = {
	"cd",
	"exit"
};

int (*builtin_func[]) (char **) = {
	&cd_builtin,
	&exit_builtin
};

//returns number of builtin functions
int num_builtins() {
	return sizeof(builtin_str) / sizeof(char *);
}

//Implementation for cd builtin
int cd_builtin(char **args)
{
	//error handlin if no parameter for cd
	if (args[1] == NULL) {
		fprintf(stderr, "wrong parameter in cd\n");
	} else {
		//changes directory with parameter from command line
		if (chdir(args[1]) != 0) {
			perror("error: could not change directory\n");
		}
	}
	return 1;
}

//Implementation for exit buildin
int exit_builtin(char **args)
{
	//stops loop by returning 0
	return 0;
}

//define fuction to use it in run_pipe_exec
int search_exec(char **args, int pipe_set, int pipe_set2);

int run_pipe_exec(char **args, char* path, char* pipe_exec) {
	
	char **pipe_args;
	int fd[2];
	pid_t childpid;
	char *readbuffer = (char *)  malloc(10000 * sizeof(char));

	if(readbuffer == NULL){
		printf("error: malloc readbuffer");
		exit(1);
	}

	pipe(fd);

	if((childpid = fork()) == -1) {
		perror("error: fork pipe");
		exit(1);
	}

	wait_child = 1;

	//child
	if(childpid == 0) {
		dup2(fd[1], 1);
		if (execv(path, args) == -1) {
			perror("error: fork pipe (execv)");
		}
	}
	//parent
	else {
		dup2(fd[0], 0);
		close(fd[1]);

		//read input from first executable
		read(fd[0], readbuffer, sizeof(readbuffer));

		pipe_args = (char **) malloc((sizeof(char *)) * (2));

		if(pipe_args==NULL){
			printf("errore: malloc pipe_args");
			exit(1);
		}

		pipe_args[0] = pipe_exec;
		pipe_args[1] = readbuffer;
		
		//run second executable of pipe
		search_exec(pipe_args, 1, 0);
		wait_child = 0;
	}
	
	return 1;
}

//run executable with path from search_exec
int run_exec(char **args, char* path, int pipe_set) {

        int status;
        int pid;
        char *pipe_exec;

	//to count pipes in input from command line
	int pipe_count = 0;
	int i;
	for (i = 0; i < sizeof(args); i++) {	
		if (args[i] != NULL) {
			if (pipe_set == 1) {
				//search for pipe in args
				if (strchr(args[i], '|') != NULL) {
					pipe_count++;
				}
			}
		}
	}

	if (pipe_count > 1) {
		perror("to much pipes\n");
		return 1;
	}
	
	// run child process
	pid = fork();
	if(pid == 0) {
		if (pipe_count == 1) {
	
			for (i = 0; i < sizeof(args); i++) {
               			 if (args[i] != NULL) {
                        		if (strchr(args[i], '|') != NULL) {
                                		args[i] = NULL;
						//set second executable of pipe
						pipe_exec = args[i+1];
                       			 } 
                		}       
        		}
	
			//run pipe
			run_pipe_exec(args, path, pipe_exec);
		} else {
			//run executable
			if (execv(path, args) == -1) {
				perror("fork error: 1\n");
			}
		}
	} else if (pid < 0) {
		perror("fork error: 2\n");
	} else {
		do {
			waitpid(pid, &status, WUNTRACED);
		} while (!WIFEXITED(status) && !WIFSIGNALED(status));
	}

	return 1;
}


//check if executable exists/is executable and set path
int search_exec(char **args, int pipe_set, int pipe_set2)
{
	int executable_exists = 0;

	int i;

	int exec_done=0;	
	//search PATH for executable and set path variable, if it exists or is executable
        for (i = 0; i < sizeof(standard_locations_path); i++)
        {
			if(exec_done){
				return 1;
			}
                if (standard_locations_path[i] != NULL) {
			char *concat = standard_locations_path[i];
			//reset concat for execution of second pipe execuatable
			if (pipe_set == 1) {
                                concat[strlen(concat)-strlen(args[0])-1] = '\0';
                        }
			concat = strcat(concat, "/");
			concat = strcat(concat, args[0]);
			
			//check if executable is existing by using access()
			if(access(concat, F_OK) != -1) {
				executable_exists = 1;
			} 
			//check if executable is executable by using access()
			if(access(concat, X_OK) != -1) {
				if (pipe_set ==1) {
					//if current executable is second executable from piperun it here
					if (execv(concat, args) == -1) {
						perror("fork error: 1\n");
					}
				}
				run_exec(args, concat, pipe_set2);
				exec_done=1;
				executable_exists = 2;
			}
			//reset concat
			concat[strlen(concat)-strlen(args[0])-1] = '\0';
		}
        }

	//search HOME for executable and set path variable, if it exists or is executable
	for (i = 0; i < sizeof(standard_locations_home); i++)
        {
			if(exec_done){
				return 1;
			}
                if (standard_locations_home[i] != NULL) {
                        char *concat = standard_locations_home[i];
			//reset concat for execution of second pipe execuatable
			if (pipe_set == 1) {
				concat[strlen(concat)-strlen(args[0])-1] = '\0';
			}
			concat = strcat(concat, "/");
                        concat = strcat(concat, args[0]);
                        //check if executable is existing by using access()
			if(access(concat, F_OK) != -1) {
				executable_exists = 1;
                        }
			//check if executable is executable by using access()
			if(access(concat, X_OK) != -1) {	
				if (pipe_set ==1) {
					//if current executable is second executable from pipe run it here
					if (execv(concat, args) == -1) {
                                		perror("fork error: 1\n");
                        		}
				}

				run_exec(args, concat, pipe_set2);
				exec_done=1;
				executable_exists = 2;
                        }
			//reset concat
                        concat[strlen(concat)-strlen(args[0])-1] = '\0';
                }
        }

	if(executable_exists != 1 && executable_exists != 2) {
		printf("executable does not exist\n");
	}
	if (executable_exists == 1) {
		printf("executable is not executable\n");
	}
	
	return 1;
}

//parse user input
int execute(char **args, int pipe_set)
{
	int i;

	//check if user input is empty
	if (args[0] == NULL) {
		printf("Input empty\n");
		return 1;
	}

	//check if input command equals builtin function
	for (i = 0; i < num_builtins(); i++) {
		if(strcmp(args[0], builtin_str[i]) == 0) {
			//run builtin fuction with command line parameter
			return (*builtin_func[i])(args);
		}
	}
	
	return search_exec(args, 0, pipe_set);
}

//Implementation for exit builtin

int main(int argc, char *argv[])
{
	char inputString[MAXCOM];
	char* input;
	int status;
	char **tokenized_str;
	int pipe_set;

	//set environment variables
	standard_locations_path = tokenize(getenv("PATH"), ":");
	standard_locations_home = tokenize(getenv("HOME"), "");

	start_shell();

	do {	
		if (wait_child == 0) {
			//print current dir
			printDirPrompt();
		
			//get input
			input = getInput(inputString);	
		
			//check if input contains a pipe
			if (strchr(input, '|') != NULL) {
		       		pipe_set = 1;
			} else {
				pipe_set = 0;
			}

			//check if input is empty
			if (input[0] == '\0') {
				status = 1;
			} else {
				//tokenize input
				tokenized_str = tokenize(input, " ");
				//execute input
				status = execute(tokenized_str, pipe_set); 
			}
		}
	} while (status);
}
