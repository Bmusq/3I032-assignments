/* NAME:Musquer Basile, Boehm Maximilian
   NSID:bam857, mab728
   Student Number:11287646, 11288097

   University of Saskatchewan
   CMPT 332 Term 1 2019
   Assignment 1
   Part A3
*/

#include <square.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <pthread.h>
#include <unistd.h>

/*
//Data structure for threads
typedef struct MyData {
       	int val1;
	int val2;
} MYDATA, *PMYDATA;
*/

int *counters;
clock_t *times;

int nbThread=0;

//to store current status of thread
int *running;

void *childMain(void *size) {
	clock_t begin = clock();
	printf("In child thread\n");

	int counter =nbThread++;

	int square_size=*((int*)size);

	//check parameters
	if(square_size<= 0) {
               fprintf(stderr, "Invalid size!");
               exit(1);
        }

	for(int i=1; i<=square_size;i++){
		Square(i);	
		counters[counter]++;
		
		if(i==square_size) {	
			clock_t end = clock();
			double totalT = (double)(end-begin);
			printf("Running time: %f\n", totalT);
			printf("Child thread finished\n");
			running[counter]=1;
			pthread_exit(NULL);
		}
	}

	return 0;
}

//create threads and call child_main for every created thread
int parentMain(int thread, int deadline, int size) {

       	//check thread parameter
	if(thread <= 0) {
        	fprintf(stderr, "Invalid number of threads!");
        	exit(1);
	}

	if(deadline <= 0) {
        	fprintf(stderr, "Invalid deadline!");
        	exit(1);
	}

	printf("Parameters checked\n");
		
	pthread_t threads[thread];
	int rc;

	printf("Creating threads\n");

	for(int i=0;i<thread;i++) {
		rc=pthread_create(&threads[i], NULL, childMain, &size);
		times[i] = clock();
		if (rc) {
			printf("ERROR; return code from pthread_create() is %d\n", rc);
			exit(-1);
		}
		printf("Thread: %d",i);
		printf(" created\n");
	}
	printf("Threads created succesfully\n");
	
	sleep(deadline);
	
	for(int i=0;i<thread;i++) {
		clock_t end = clock();
	       	double totalT =	(double)(end-times[i]);
		if(times[i]==0) {
			printf("Running time: %f\n", totalT);
		}	
		if(running[i]==0) {
			pthread_cancel(threads[i]);
			printf("Child thread killed\n");
		}
	}

	for(int j=0;j<thread;j++) {
			printf("Invocations of square: %d\n", counters[j]);
		}
	
        return 0;
}


//get commandline parameters and call parentMain
int main(int argc, char *argv[]){

	assert(argv[1] != NULL);
	assert(argv[2] != NULL);
	assert(argv[3] != NULL);

	int thread = atoi(argv[1]);
	int deadline = 	atoi(argv[2]);
	int size = atoi(argv[3]);

	counters= (int *) malloc(thread*sizeof(int));
	times = (clock_t *) malloc(thread*sizeof(clock_t));
	int i;
	for(i=0; i<thread;i++){
			times[i]=0;
			counters[i]=0;
	}

	running= (int *) malloc(thread*sizeof(int));
	for(i=0; i<thread;i++) {
		running[i]=0;
	}

	printf("In parent thread\n");

	parentMain(thread, deadline, size);
      	
	printf("Programm finished\n");

        return 0;
}
