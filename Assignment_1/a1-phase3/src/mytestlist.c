/* NAME:Musquer Basile, Boehm Maximilian
   NSID:bam857, mab728
   Student Number:11287646, 11288097
   Group: 34

   University of Saskatchewan
   CMPT 332 Term 1 2019
   Assignment 1
   Part C
*/

/* Include */
#include <list.h>

/* Define for the dataType */
#define CAST (int *)
#define TYPE int

/* Extern variables */
extern NodeA* currNA;
extern ListA* currLA;

/* Structure */
typedef struct _list_ind ListInd;
struct _list_ind{
	List* list;
	int ind;
};
/********************* User Functions ****************************/
/****************************************************************/
/*
In this part, the user implement his displaying, comparating and itemFreeing functions
acoording to the dataType he is using.

He must also set the correct cast for his dataType in the define section.

For the sake of the testing program, we will consider that the dataType is set to int.
Note: is the dataType is something else than int, with the provided user functions, the test programm
	will not run correctly
*/
int comparator(void* a, void* b){
	/* With ints the comparison test is very easy to do but if the dataType is a struct 
	which contains a struct which contains strings (for instance), it can be very tricky
	*/

	TYPE* pt_a= CAST a;
	TYPE* pt_b= CAST b;

	if(*pt_a==*pt_b) return 1;
	return 0;
}

void itemFree(void* item){
	/* itemFree is useless if the dataType considered is int
	It makes sense when the item is dynamicly allocated, thus a call to the
	function free( CAST item) is require 

	However if every item is allocated dynamicly, this function should be needed in every
	functions within list_removers.c 
	*/

	printf("%d has been succefully freed\n", *(CAST(item)));	
	return ;
}

void displayItem(void* item, FILE *f){
	/* With ints displaying an item is very easy to do but if the dataType is a struct 
	which contains a struct which contains strings, it can be very tricky requiring
	multiples for loop->

	Note: This version of displayItem displays only one item at a time, to display a
	array of items you shall use for loop within the main function
	*/

	fprintf(f, "%d\t", *(CAST(item)));
	return ;
}



/********************* Managing Functions ****************************/
/********************************************************************/
/*
These functions are here to help the user manage his array of lists and items

The versions below of these functions can be edited at the user ease.
He must remain careful of segfault
*/

void displayLind(ListInd lind, FILE* f ){
	lind.list->curr=lind.list->first;
	fprintf(f, "Order of the items within the list(%d)\n", lind.ind);
	while(lind.list->curr != NULL){
		fprintf(f,"%d\t", *(CAST(lind.list->curr->item)));
		lind.list->curr=lind.list->curr->next;	
	}
	lind.list->curr=lind.list->last;
	fprintf(f,"\n");
	return;
}

void displayArrayLinds(ListInd arrayLind[MAX_LIST], FILE* f){
	int i;
	fprintf(f, "Max lists = %d\n", MAX_LIST);
	for(i=0; i<MAX_LIST ;i++){
		if(arrayLind[i].ind==-1)
			fprintf(f, "List at index %d unused\n", i);
		else{
			fprintf(f, "List(%d) at index %d used\n", arrayLind[i].ind, i);
		}
	}
	return ;
}

void freeLind(ListInd* lind){
	lind->ind=-1;
	lind->list=NULL;
}

int addList(ListInd arrayLind[MAX_LIST], int x){
	unsigned int i;
	for(i=0; i<MAX_LIST; i++){
		if(arrayLind[i].ind==-1){
			arrayLind[i].ind=x;
			arrayLind[i].list=ListCreate();
			return 1;
		}
	}
	return 0;	
}
/**********************************************************************************/
/********************* MAIN TEST FUNCTION *****************************************/
/**********************************************************************************/

int main(int argc, char *arv[]){
	/* Openning the test.results file */
	FILE *f;
	f=fopen("./documentation/PartC.testresults.txt", "w");

/******************* Creating lists *******************/
	unsigned int i;
	ListInd arrayLind[MAX_LIST];
	for(i=0; i<MAX_LIST;i++){
		arrayLind[i].ind=-1;
		arrayLind[i].list=NULL;
	}

	fprintf(f, "Creating two list. .\n");
	addList(arrayLind, 1);
	addList(arrayLind, 2);
	displayArrayLinds(arrayLind, f);

	fprintf(f, "\n");
/******************* Creating items *******************/
	void* tabItem[MAX_NODE+3];
	unsigned int counter=0;
	TYPE tabInt[MAX_NODE+3];
	for(i=0;i<MAX_NODE+3;i++){
		tabInt[i]=i;
		tabItem[i]=&tabInt[i];
	}

/*
To add, prepend, insert, append an item in the list we will use this notation :
	tabItem[counter++];


It should never lead to a segfault as the size of the number of item is bigger than the max number of node. Using the statement above like this:
	ListAdd(arrayLind[index].list, tabItem[counter++]);
should prevent segfault

Note: dont forget to decrease counter each time a node is removed

It is a suggestion how to implement the adding of items in a list, thus you can manage this differently for example if you are afraid you might forget to decrease the counter
*/
/***************************************General Test****************************/
	fprintf(f, "\t\t\t\t\t GENERAL TESTs\n\n\n");
	fprintf(f, "/--------------------- Test list_adders.c----------------------------\n");

	fprintf(f, "\n");
	//Putting items into list 1 
	ListAdd(arrayLind[0].list, tabItem[counter++]);
	fprintf(f, "Adding %d in the list(%d)\t Adding in an empty list\n", *(CAST(arrayLind[0].list->curr->item)), arrayLind[0].ind);

	ListInsert(arrayLind[0].list, tabItem[counter++]);
	fprintf(f, "Insert %d in the list(%d)\t Inserting in the front of a list\n", *(CAST(arrayLind[0].list->curr->item)), arrayLind[0].ind);

	ListAppend(arrayLind[0].list, tabItem[counter++]);
	fprintf(f, "Append %d at the end of the list(%d)\n", *(CAST(arrayLind[0].list->curr->item)), arrayLind[0].ind);

	ListPrepend(arrayLind[0].list, tabItem[counter++]);
	fprintf(f, "Prepend %d at the front of the list(%d)\n", *(CAST(arrayLind[0].list->curr->item)), arrayLind[0].ind);

	ListAdd(arrayLind[0].list, tabItem[counter++]);
	fprintf(f, "Adding %d in the list(%d)\t Adding in the middle of a list\n", *(CAST(arrayLind[0].list->curr->item)), arrayLind[0].ind);

	ListInsert(arrayLind[0].list, tabItem[counter++]);
	fprintf(f, "Insert %d in the list(%d)\t Inserting in the middlfe of a list\n", *(CAST(arrayLind[0].list->curr->item)), arrayLind[0].ind);

	fprintf(f, "\n");
	//Display link between node
	fprintf(f, "Displaying link betwwen nodes of list(%d)\n", arrayLind[0].ind);
	ListFirst(arrayLind[0].list);
	while(arrayLind[0].list->curr !=NULL){
		fprintf(f, "Previous : %p\t", arrayLind[0].list->curr->prev);
		fprintf(f, "Cuurent : %p\t", arrayLind[0].list->curr);
		fprintf(f, "Next : %p\n", arrayLind[0].list->curr->next);
		arrayLind[0].list->curr=arrayLind[0].list->curr->next;
	}

	fprintf(f, "\n");
	//Display content of node
	ListFirst(arrayLind[0].list);
	fprintf(f, "Displaying content of the nodes from list(%d):\n", arrayLind[0].ind);
	fprintf(f, "\t\t\t");
	while(arrayLind[0].list->curr !=NULL){
		if(arrayLind[0].list->curr->prev !=NULL)
			fprintf(f, "Previous : %d\t\t", *(CAST(arrayLind[0].list->curr->prev->item)));
		fprintf(f, "Current : %d\t\t", *(CAST(arrayLind[0].list->curr->item)));
		if(arrayLind[0].list->curr->next !=NULL)
			fprintf(f, "Next : %d\t\t", *(CAST(arrayLind[0].list->curr->next->item)));

		fprintf(f, "\n");
		arrayLind[0].list->curr=arrayLind[0].list->curr->next;
	}


	fprintf(f, "\n");
	fprintf(f, "/-----------------Test list_movers.c---------------------/\n");
	//Moving cursor in the list
	displayLind(arrayLind[0], f);
	fprintf(f, "Number of item: %d\n", (ListCount(arrayLind[0].list)));
	fprintf(f, "First item: %d\n", *(CAST(ListFirst(arrayLind[0].list))));
	fprintf(f, "Next (after first) item: %d\n", *(CAST(ListNext(arrayLind[0].list))));
	fprintf(f, "Last item: %d\n", *(CAST(ListLast(arrayLind[0].list))));
	fprintf(f, "Prev (before Last) item: %d\n", *(CAST(ListPrev(arrayLind[0].list))));
	fprintf(f, "Curr (same one) item: %d\n", *(CAST(ListCurr(arrayLind[0].list))));
	fprintf(f, "\n");
	//Search an item in the list
	int arg=0;
	void* parg=&arg;
	fprintf(f, "Search %d:\n", arg);
	void* test=ListSearch(arrayLind[0].list, comparator, parg);
	if(test ==NULL)
		fprintf(f, "%d not found\n", arg);
	else
		fprintf(f, "%d has been found\n", *(CAST test));

	arg=3;
	fprintf(f, "Search %d:\t", arg);
	fprintf(f, "As curremt item is 0, after 3 in the list, ListSearch() should not find %d, moreover curr item should be the last item of the list\n", arg);
	test=ListSearch(arrayLind[0].list, comparator, parg);
	if(test ==NULL)
		fprintf(f, "%d not found\n", arg);
	else
		fprintf(f, "%d has been found\n", *(CAST test));


	fprintf(f, "\n");
	fprintf(f, "/---------------------Test list_remover.c---------------------/\n");
	displayLind(arrayLind[0], f);

	//Remove a node
	ListPrev(arrayLind[0].list);// Set the current not to the last one
	fprintf(f, "Curr item: %d\n", *(CAST(ListCurr(arrayLind[0].list))));
	fprintf(f, "Item removed: %d\n", *(CAST(ListRemove(arrayLind[0].list))));
	displayLind(arrayLind[0], f);
	counter--;

	//Trim a node
	fprintf(f, "Last item: %d\n", *(CAST(ListLast(arrayLind[0].list))));
	fprintf(f, "Item trimed: %d\n", *(CAST(ListTrim(arrayLind[0].list))));
	displayLind(arrayLind[0], f);
	counter--;
	
	//Number of list available before Concat
	ListA* tmpLA=currLA;
	int countList=0;
	while(tmpLA!=NULL){
		countList ++;
		tmpLA=tmpLA->nextLA;
	}
	fprintf(f, "Max number of lists: %d\nNumber of lists available before (should be %d): %d\n", MAX_LIST, 1, countList);

	fprintf(f, "\n");
	//Concat two list
	ListInsert(arrayLind[1].list, tabItem[counter++]);
	fprintf(f, "Insert %d in the list(%d)\tInserting in an empty a list\n", *(CAST(arrayLind[1].list->curr->item)), arrayLind[1].ind);
	
	fprintf(f, "Concat list(%d) to list(%d)\n", arrayLind[1].ind, arrayLind[0].ind);
	ListConcat(arrayLind[0].list, arrayLind[1].list);
	displayLind(arrayLind[0], f);

	fprintf(f, "Ind of list(%d) is set to -1 because the list has been set available again\n", arrayLind[1].ind);
	freeLind(&arrayLind[1]);
	displayArrayLinds(arrayLind, f);

	fprintf(f, "\n");
	//Number of list available after Concat
	tmpLA=currLA;
	countList=0;
	while(tmpLA!=NULL){
		countList ++;
		tmpLA=tmpLA->nextLA;
	}
	fprintf(f, "Max number of lists: %d\nNumber of lists available after Concat (should be %d): %d\n", MAX_LIST, 2, countList);

	fprintf(f, "\n");
	//List Free
	fprintf(f, "Freeing list(%d). .\n", arrayLind[0].ind);
	for(i=0; i<arrayLind[0].list->nbNode; i++) counter--;
	ListFree(arrayLind[0].list, itemFree);
	
	freeLind(&arrayLind[0]);
	displayArrayLinds(arrayLind, f);


	fprintf(f, "\n");
	//Number of node available in the end
	NodeA* tmpNA=currNA;
	int countNode=0;
	while(tmpNA!=NULL){
		countNode ++;
		tmpNA=tmpNA->nextNA;
	}
	fprintf(f, "Max number of nodes: %d\nNumber of nodes available (should be %d): %d\n", MAX_NODE, MAX_NODE, countNode);

	fprintf(f, "\n");
	//Number of list available in the end
	tmpLA=currLA;
	countList=0;
	while(tmpLA!=NULL){
		countList ++;
		tmpLA=tmpLA->nextLA;
	}
	fprintf(f, "Max number of lists: %d\nNumber of lists available (should be %d): %d\n", MAX_LIST, MAX_LIST, countList);


/*******************Test Cases*******************/
/***********************************************/
	fprintf(f, "\n\n\n--------------------------------------------------------------------------------------------\n");
	fprintf(f, "\t\t\t\t\t TESTs CASES");

	fprintf(f, "\nCreating two new lists\n");
	addList(arrayLind, 3);
	addList(arrayLind, 4);
	displayArrayLinds(arrayLind, f);

	fprintf(f, "\n");
	//Appending in an empty list(3)
	ListAppend(arrayLind[0].list, tabItem[counter++]);
	fprintf(f, "Appending %d in the list(%d)\t Appending in an empty list\n", *(CAST(arrayLind[0].list->curr->item)), arrayLind[0].ind);

	//Return Next item when next==Null
	fprintf(f, "Next (after first) item (one node list): ");
	if((CAST(ListNext(arrayLind[0].list))) ==NULL)
		fprintf(f, " none\n");
	else
		fprintf(f, " err\n");	

	fprintf(f, "\n");
	//Prepending in an empty list(4)
	ListPrepend(arrayLind[1].list, tabItem[counter++]);
	fprintf(f, "Prepend %d in the list(%d)\t Prepending in the front of a list\n", *(CAST(arrayLind[1].list->curr->item)), arrayLind[1].ind);

	//Return previous item when previous==Null
	fprintf(f, "Previous (before first) item (one node list): ");
	if((CAST(ListNext(arrayLind[1].list))) == NULL )
		fprintf(f, " none\n");
	else
		fprintf(f, " err\n");	

	fprintf(f, "\n");
	//Adding at the end of list(3)
	ListAdd(arrayLind[0].list, tabItem[counter++]);
	fprintf(f, "Add %d at the end of the list(%d)\n", *(CAST(arrayLind[0].list->curr->item)), arrayLind[0].ind);

	fprintf(f, "\n");
	//Inserting at the front of a list
	ListPrepend(arrayLind[1].list, tabItem[counter++]);
	fprintf(f, "Insert %d at the front of the list(%d)\n", *(CAST(arrayLind[1].list->curr->item)), arrayLind[1].ind);

	fprintf(f, "\n");
	fprintf(f, "/---------------------Test list_remover.c---------------------/\n");
	//Remove a node when node is last
	displayLind(arrayLind[0], f);
	fprintf(f, "Last item: %d\n", *(CAST(ListLast(arrayLind[0].list))));
	fprintf(f, "Item removed from the end of the list : %d\n", *(CAST(ListRemove(arrayLind[0].list))));
	displayLind(arrayLind[0], f);
	counter--;

	fprintf(f, "\n");
	//Remove a node when node is first
	displayLind(arrayLind[1], f);
	fprintf(f, "First item: %d\n", *(CAST(ListFirst(arrayLind[1].list))));
	fprintf(f, "Item removed from the end of the list : %d\n", *(CAST(ListRemove(arrayLind[1].list))));
	displayLind(arrayLind[1], f);
	counter--;

	fprintf(f, "\n");
	//Remove a node when one node
	displayLind(arrayLind[1], f);
	fprintf(f, "Curr item: %d\n", *(CAST(ListCurr(arrayLind[1].list))));
	fprintf(f, "Item removed from one node list : %d\n", *(CAST(ListRemove(arrayLind[1].list))));
	displayLind(arrayLind[1], f);
	counter--;

	fprintf(f, "\n");
	//Concat a non empty list to an empty list
	fprintf(f, "Concat list(%d) to list(%d)\n", arrayLind[0].ind, arrayLind[1].ind);
	ListConcat(arrayLind[1].list, arrayLind[0].list);
	displayLind(arrayLind[1], f);

	fprintf(f, "Ind of list(%d) is set to -1 because the list has been set available again\n", arrayLind[0].ind);
	freeLind(&arrayLind[0]);
	displayArrayLinds(arrayLind, f);


	fprintf(f, "\n");
	//Trim a node when one node
	displayLind(arrayLind[1], f);
	fprintf(f, "Curr item: %d\n", *(CAST(ListCurr(arrayLind[1].list))));
	fprintf(f, "Item trimed from one node list: %d\n", *(CAST(ListTrim(arrayLind[1].list))));
	displayLind(arrayLind[1], f);
	counter--;

	fprintf(f, "\n");
	//Trim a node when list empty
	fprintf(f, "Item trimed from an empty list : ");
	if((CAST(ListTrim(arrayLind[1].list)))==NULL){
		fprintf(f, " no item in the list, trim failed\n");
	}
	else
		fprintf(f, "err\n");
	fprintf(f, "\n");
	//Remove a node when list empty
	fprintf(f, "Item removed from an empty list : ");
	if((CAST(ListRemove(arrayLind[1].list)))==NULL){
		fprintf(f, " no item in the list, removed failed\n");
	}
	else
		fprintf(f, "err\n");

	
	fprintf(f, "\n");
	fprintf(f, "/---------------------Test maximum size---------------------/\n");


	//Number of list available
	tmpLA=currLA;
	countList=0;
	while(tmpLA!=NULL){
		countList ++;
		tmpLA=tmpLA->nextLA;
	}
	fprintf(f, "Number of lists available: %d\n", countList);
	//Exceeding maximum number of lists
	fprintf(f, "Exceeding max number of lists . .");
	for(i=0; i<countList+1; i++){
		if(addList(arrayLind, i+10)==0){
			fprintf(f, "\nMax number of list reached\n");
			break;
		}
	}

	fprintf(f, "\n");
	//Number of node available
	tmpNA=currNA;
	countNode=0;
	while(tmpNA!=NULL){
		countNode ++;
		tmpNA=tmpNA->nextNA;
	}
	fprintf(f, "Number of nodes available : %d\n", countNode);
	//Exceeding maximum number of nodes
	fprintf(f, "Exceeding max number of nodes. . \n");
	int tmp=9;
	for(i=0; i<countNode; i++){
		if(ListAdd(arrayLind[0].list, &tmp)==-1){
			fprintf(f, "Max number of node reached\n");
			break;
		}
	}
	tmpNA=currNA;
	countNode=0;
	while(tmpNA!=NULL){
		countNode ++;
		tmpNA=tmpNA->nextNA;
	}
	fprintf(f, "Number of nodes available : %d\n", countNode);

	fprintf(f, "\nEND OF PROGRAM NO INTERRUPTION\n");
	fclose(f);
	return 0;
}
