/* NAME:Musquer Basile, Boehm Maximilian
 * NSID:bam857, mab728
 * Student Number:11287646, 11288097
 * Group: 34
 *
 * University of Saskatchewan
 * CMPT 332 Fall 2019
 * File: LibList:list_create.c
*/

/************************** PRE-SETs ******************************/
/* Include */
#include <list.h>

/* Array of Struct List*/
List chunkList_pt[MAX_LIST];

/* Array of List_Available */
ListA arrayListA[MAX_LIST];

/* Pointer to my available list*/
ListA* currLA=NULL;

/* Init */
static unsigned int initList=0;


/************************** FUNCTIONS ******************************/
/******************************************************************/

/*-------------------------------*//*List Create*/
List* ListCreate() {
	assert( chunkList_pt != NULL );
	assert( arrayListA != NULL );

	List* tmpList = NULL;
	ListA* tmpListA = NULL;

	/* Begining Initialisation */
	int i=0;
	if ( initList == 0 ) {
		for ( tmpListA = arrayListA, tmpList = chunkList_pt, i = 0; tmpListA < (arrayListA + MAX_LIST - 1); tmpListA++, tmpList++, i++ ) {
			tmpListA->ptrList = tmpList;
			tmpList->locate = i;
			tmpListA->nextLA = tmpListA + 1;
		}
		tmpListA->ptrList = tmpList;
		tmpList->locate = i;
		tmpListA->nextLA = NULL;
		currLA = arrayListA;

		initList = 1;
	}

	/* Test Maximum */
	if ( currLA == NULL ) {
		printf( "Maximum number of lists reach, could not create a new one\n" );
		return NULL;
	}

	/* Find available list */
	tmpList = currLA->ptrList;
	tmpListA = currLA;
	currLA = currLA->nextLA;
	tmpListA->nextLA = NULL;

	if ( tmpList == NULL ) {
		printf( "Linking between pool of lists and array of available lists failed\n" );
		return tmpList;
	}	
	
	//Variables Initialisation
	tmpList->curr = NULL;
	tmpList->first = NULL;
	tmpList->last = NULL;
	tmpList->nbNode = 0;

	return tmpList;
}

