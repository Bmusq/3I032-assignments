#  NAME:Musquer Basile, Boehm Maximilian
#  NSID:bam857, mab728
#  Student Number:11287646, 11288097
#  University of Saskatchewan
#  CMPT 332 Term 1 2019
#  Assignment 1
#  Part B

#!/bin/bash

#Declare variables
OS=$(uname)
MSwin="MINGW64_NT-10.0-17763"
chosen_prog=$1
choice="c"

names=( Thread Deadline Size)
parameterArray=( -1 -1 -1)
i=1
num_rows=0


FILE="${2:-/dev/stdin}"
bool_input=0

echo "Welcome in the interactive menu to run part A programs"
if [ -f "${2:-/dev/stdin}" ] ; then
	bool_input=1
fi

while [ "$choice" == "c" ]; do

if [ "$chosen_prog" == "A1" ] ; then
	echo "You chose to run partA1 prog"
	if [ "$OS" == "Linux" ] ; then
		echo "You are not on a Windows Machine"
		echo "The program can not be executed"
		exit
	else
		if [ $bool_input -eq 1 ] ; then
			while IFS=' ' read -r one two three
			do
				echo ""
				if ! [ "$one" -eq "$one"  ] 2> /dev/null
				then
					echo "Sorry only integers"
					exit
				fi
				if ! [ "$two" -eq "$two"  ] 2> /dev/null
				then
					echo "Sorry only integers"
					exit
				fi
				if ! [ "$three" -eq "$three"  ] 2> /dev/null
				then
					echo "Sorry only integers"
					exit
				fi
				bash -c "./partA1 $one $two $three"					
			done < "${2:-/dev/stdin}"
			exit
		else	
			for(( i=0;i<3;i++ )); do
				while [ ${parameterArray[i]} -eq -1 ]; do
				echo "Parameter ${names[${i}]} = ?"
				read parameterArray[i]
				if ! [ "${parameterArray[i]}" -eq "${parameterArray[i]}" ] 2> /dev/null
				then
					echo "Error procedure A1 : invalid parameter ${names[${i}]}"
					parameterArray[i]=-1
				fi
				done
			done
			bash -c "./partA1 ${parameterArray[0]} ${parameterArray[1]} ${parameterArray[2]}"

		fi
	fi
else

	if [ "$chosen_prog" == "A2" ] ; then
		echo "You chose  to run partA2 prog"
		if [ $bool_input -eq 1 ] ; then
			while IFS=' ' read -r one two three
			do
				echo ""
				if ! [ "$one" -eq "$one"  ] 2> /dev/null
				then
					echo "Sorry only integers"
					exit
				fi
				if ! [ "$two" -eq "$two"  ] 2> /dev/null
				then
					echo "Sorry only integers"
					exit
				fi
				if ! [ "$three" -eq "$three"  ] 2> /dev/null
				then
					echo "Sorry only integers"
					exit
				fi
				bash -c "./partA2 $one $two $three"					
			done < "${2:-/dev/stdin}"
			exit
		else	
			for(( i=0;i<3;i++ )); do
					while [ ${parameterArray[i]} -eq -1 ]; do
					echo "Parameter ${names[${i}]} = ?"
					read parameterArray[i]
					if ! [ "${parameterArray[i]}" -eq "${parameterArray[i]}" ] 2> /dev/null
					then
						echo "Error procedure A2 : invalid parameter ${names[${i}]}"
						parameterArray[i]=-1
					fi
					done
			done
			bash -c "./partA2 ${parameterArray[0]} ${parameterArray[1]} ${parameterArray[2]}"
		fi
	else

		if [ "$chosen_prog" == "A3" ] ; then
			echo "You chose  to run partA3 prog"
			if [ $bool_input -eq 1 ] ; then
				while IFS=' ' read -r one two three
				do
					echo ""
					if ! [ "$one" -eq "$one"  ] 2> /dev/null
					then
						echo "Sorry only integers"
						exit
					fi
					if ! [ "$two" -eq "$two"  ] 2> /dev/null
					then
						echo "Sorry only integers"
						exit
					fi
					if ! [ "$three" -eq "$three"  ] 2> /dev/null
					then
						echo "Sorry only integers"
						exit
					fi
					bash -c "./partA3 $one $two $three"					
				done < "${2:-/dev/stdin}"
				exit
			else	
				for(( i=0;i<3;i++ )); do
					while [ ${parameterArray[i]} -eq -1 ]; do
					echo "Parameter ${names[${i}]} = ?"
					read parameterArray[i]
					if ! [ "${parameterArray[i]}" -eq "${parameterArray[i]}" ] 2> /dev/null
					then
						echo "Error procedure A3 : invalid parameter ${names[${i}]}"
						parameterArray[i]=-1
					fi
					done
				done
				bash -c "./partA3 ${parameterArray[0]} ${parameterArray[1]} ${parameterArray[2]}"
			fi
		else

			if [ "$chosen_prog" == "A4" ] ; then
				echo "You chose  to run partA4 prog"
				echo "Executing partA4"
				#Execute partA4
			else

				if [ "$chosen_prog" == "A0" ] ; then
					echo"This program does not exist"
				else 
					echo""
				fi
			fi
		fi
	fi
fi

#Aking for quiting or continuing
choice="x"
while [ "$choice" == "x" ]; do

echo ""
echo "Do you want to continue or quit the script [c, q]?"
echo "c. continue"
echo "q. quit"
read choice
if [ "$choice" == "c" ] ; then
	echo "You choose to continue"
	parameterArray=( -1 -1 -1)
	chosen_prog="A0"
	echo "Choose a program to run [A1, A2, A3, A4]?"
	while [ "$chosen_prog" == "A0" ]; do
	read chosen_prog
	if [ "$chosen_prog" == "A1" ] ; then
		continue
	else
		if [ "$chosen_prog" == "A2" ] ; then
			continue
		else
			if [ "$chosen_prog" == "A3" ] ; then
				continue
			else
				if [ "$chosen_prog" == "A4" ] ; then
					continue
				else 
					echo "Incorrect input. Try again"
                			chosen_prog="A0"
				fi
			fi
		fi
	fi
	done
else

	if [ "$choice" == "q" ] ; then
		echo "You choose to close the script"
		exit
	else
		echo "Enter a valid caracter"
		choice="x"
	fi
fi
done
done
